<?php

  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>

<!-- codes starts here -->



     <div class="panel panel-default output">
        <div class="panel-heading">
          <h3></h3>
        </div>
        <br/>
       
          
          <div class="panel-body">
            <!-- CONTENT BODY HERE -->
            <div class="row">
                
                <div class="col-md-12">
                  <div class="pull-right">
                    <button class="btn btn-primary btn-lg" id="btnaddinstruction" onClick="showinstruction()">Add Instruction</button>
                    <button class="btn btn-default btn-lg" id="btnaddnew" onClick="showaddform()">Add</button>
                  </div>
                  <div  id="savealert" class="alert alert-success" align="center" style="height:200px;position:fixed;z-index:2;left:40%;right:40%;top:10%;display:none;">
                    <h1 id="savetext" style="position:absolute;top:35%;left:50%;transform: translateX(-50%) translateY(-35%);"></h1>
                  </div>
                  <?php
                  	
                  ?>

                  <br/>
                  <br/>
                  <br/>

                    <div id="light" class="white_content">
                      <p align="right">
                        <a href = "javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">X close</a>
                      </p>
                      <div id="editquest-cont">
                      </div>
                    </div>
                  <div id="fade" class="black_overlay"></div>

                  <div class="datalist" style="height:300px;overflow:scroll;">


    <?php
    //$id = $_GET['tqid'];
    $id2 = $_SESSION['SESS_USER_ID'];
    $qry = mysql_query("SELECT * FROM qmasterlist");


    echo '<table class="table table-striped table-hover" style="width:1600px;">
        <tr>
          <th>Question ID </th>
          <th>Question No </th>
          <th>Question </th>
          <th>Answer </th>
          <th>Choice1 </th>
          <th>Choice2 </th>
          <th>Choice3 </th>
          <th>Choice4 </th>
          <th>Choice5 </th>
          <th></th>
        </tr>
        ';

    $label = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V',
            'W','X','Y','Z');
    $q='';
    $c2='';
    $c3='';
    $c4='';
    $c5='';
    $c1='';
    while ($result= mysql_fetch_array($qry)) {
      if($result['qimg']=='undefined' || $result['qimg']==''){
        $q = $result['qtext'];
      }else{
        $q = $result['qimg'];
      }

      if($result['a']=='' || $result['a']=='undefined'){
        $c1 = $result['a_img'];
      }else{
        $c1 = $result['a'];
      }

      if($result['b']=='' || $result['b']=='undefined'){
        $c2 = $result['b_img'];
      }else{
        $c2 = $result['b'];
      }

      if($result['c']=='' || $result['c']=='undefined'){
        $c3 = $result['c_img'];
      }else{
        $c3 = $result['c'];
      }

      if($result['d']=='' || $result['d']=='undefined'){
        $c4 = $result['d_img'];
      }else{
        $c4 = $result['d'];
      }

      if($result['e']=='' || $result['e']=='undefined'){
        $c5 = $result['e_img'];
      }else{
        $c5 = $result['e'];
      }

      echo '<tr>
        <td>'.$result['qid'].'</td>
        <td>'.$result['qno'].'</td>
        <td>'.$q.'</td>
        <td>'.$result['answer'].'</td>
        <td>'.$result['a_label']. '. ' .$c1.'</td>
        <td>'.$result['b_label']. '. ' .$c2.'</td>
        <td>'.$result['c_label']. '. ' .$c3.'</td>
        <td>'.$result['d_label']. '. ' .$c4.'</td>
        <td>'.$result['e_label']. '. ' .$c5.'</td>
        <td>
          <button id="btn-'.$result['qid'].'" class="btn btn-primary btn-xs" onClick="edittest('."'".$result['qid']."'".''.','."'imgq-".$result['qid']."'".','."'".$result['subjectid']."'".')">Edit</button>
          <button class="btn btn-default btn-xs" onClick="deletetest('."'".$result['qid']."'".''.','.''."'".$id2."'".')">Delete</button>
          </td>
        </tr>';
      
    }

    echo '</table>';


    ?>
                  </div>
              </div>
            		
                <div class="col-md-12">
                  <hr/>
                  <div id="instruction_cont">
                  </div>
                	<div id="subset-cont" style="display:none;">

                    <div class="form-group" id="level-group">
                      <label>Subject: <b class="text-danger">(Required)</b></label>
                      <?php
                        $subject = mysql_query("SELECT * FROM subject");
                        echo '<select id="qsubsetid" class="form-control">
                                  <option value=""></option>';
                        while ($resultsubj = mysql_fetch_array($subject)) {
                                  echo '<option value="'.$resultsubj['subjectid'].'">'.$resultsubj['subjectname'].'</option>';

                               
                        }
                        echo '</select>';

                        

                      ?>
                      
                      
                    </div>

                  	<div class="form-group" id="level-group">
  	                  <label>Question Type: <b class="text-danger">(Required)</b></label>
  	                  <select id="qtype" class="form-control" onChange="showmore(this.value)">
                        <option></option>
                        <option value="multiplec">MULTIPLE CHOICE</option>
                        <!--<option value="fillin">FILL-IN THE BLANKS</option>
                        <option value="agdis">MATCHING TYPE</option>-->
                      </select>
  	                </div>

                    <div class="hiddenobject">
                    </div>

                  </div>
              
            	  </div>
                
          </div>
          <!--<div class="panel-footer">
              <div class="row">
                <div class="col-md-12">
                  <button onClick="addlevel()" class="btn btn-primary btn-md pull-right" id="btnSave" data-toggle="tooltip" title="Click to add" data-placement="left">Add</button>          
                </div>
              </div>
          </div>-->
        </div>
      


      <!-- MODAL -->
      <div id="modal-alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
          <div class="modal-content text-center">
            <br />
            <img id="loading" src="image/loading.gif" />
            <h1 id="loading-text" class="modal-title">Saving...</h1>
          </div>
        </div>
      </div>
      <!-- MODAL -->





<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

<script src="js/navigation.js"></script>
