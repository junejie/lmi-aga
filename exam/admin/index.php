<?php	
  session_start();
  $_SESSION['LAST_ACTIVITY'] = time();
  
  if(@(isset($_SESSION['SESS_USER_ID']) || !(trim($_SESSION['SESS_USER_ID']) == ''))){
    header("location: cpanel.php");
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>LMI E-LEARNING</title>
    <link rel="shortcut icon" href="../../../assets/ico/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="../dist/flat/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">
  </head>

  <body>
    <div class="container">
	<h1 align="center">LMI E-LEARNING</h1>
        <div class="login-panel panel panel-default col-md-4 col-md-offset-4">
		
		  <div class="panel-body">

            
            <form class="form-signin text-center" method="post" role="form">
              <h2 class="form-signin-heading">Please sign in</h2>

              <div id="page-alert" class="alert alert-danger" data-alert="alert">
                <span id="page-alert-message"></span>
              </div>

                <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input type="text" class="form-control" name="username" placeholder="Username"  autofocus>
                  <span class="span-user-error"></span>
                </div>
                <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                  <input type="password" class="form-control" name="password" placeholder="Password" >
                  <span class="span-passowrd-error"></span>
                </div>
                  <br/>
                  <div class="form-group">
                    <select class="form-control" name="usertype">
                      <option value="Admin">Admin</option>
                    </select>
                  </div>
                
              <button class="form-btn btn btn-lg btn-primary btn-block" type="button">Sign in</button>
            </form>

            

          </div>
        </div>
    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../dist/jquery/jquery.min.js"></script>
    <script src="../dist/js/bootstrap.min.js"></script>
    <script src="../js/signin.js"></script>
  </body>
</html>
