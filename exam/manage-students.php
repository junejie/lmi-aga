<?php
 
  //include auth
  require_once('auth.php');//include for authorization only
   
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<style type="text/css">
.thead{
  font-weight: bold;
}
</style>
<!-- codes starts here -->
<h1><i class="nav-icons fa fa-home"></i>Students <small>Module</small></h1>
  
  <div class="table-responsive">
      <table class="table table-condensed">
        <thead>
          <tr class="thead">
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Contact #</th>
            <th>Status</th>
            <th>Manage</th>
            <td>Section</td>
          </tr>
        </thead>
        <tbody id="tableResult">
          <!-- insert members -->
          <?php include_once('proc/process-view-students.php') ?>
        </tbody>
      </table>
    </div>

    <!-- view modal -->
          <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="userModalLabel">Name Here</h4>
                </div>
                <div class="modal-body row">

                  <div class="row col-md-12">
                    <div class="col-md-5">
                      <!-- Your first column here -->
                      <span id="studentid" style="display:none;"></span>
                      <img id="modal-pic" src="" class="center-block img-responsive img-circle">

                      <form class="form-horizontal" id="upimg" action="upload.php" method="post" enctype="multipart/form-data">
                     
                        <div class="form-group">
                          <input type="file" class="btn btn-xs" name="myfile" id="uploadpic">
                          
                        </div>  
                      </form>
                      <button class="btn btn-primary btn-xs" onclick="upload('upimg')">Upload</button>
                    </div>

                    <div class="col-md-7">
                      <h1 id="modal-fullname">Full Name Here</h1>
                      <p id="modal-type" class="lead text-muted">Student</p>
                    <hr />

                    
                    <div class="row">
                      <div class="col-md-6">
                        <h4 id="modal-gender"></h4>
                        <span class="text-muted">Gender</span>
                      </div>
                      <div class="col-md-6">
                        <h4 id="modal-birthday"></h4>
                        <span class="text-muted">Birthday</span>
                      </div>
                    </div>
                    

                    </div>
                  </div>

                  <div class="row col-md-12">
                      <hr />
                      <div class="col-md-6">
                        <h4 id="modal-address">Addr</h4>
                        <span class="text-muted">Address</span>
                        <h4 id="modal-contact">Contact</h4>
                        <span class="text-muted">Contact</span>
                      </div>

                      <div class="col-md-6">
                        <h4 id="modal-email">email</h4>
                        <span class="text-muted">Email</span>
                        <h4 id="modal-status"><span id="userStatus" class="label label-default">Status</span></h4>
                        <span class="text-muted">Status</span>
                      </div>
                  </div>

                  <div class="row col-md-12">
                      <hr />
                      <div class="col-md-6">
                        <h4 id="modal-sy">school year</h4>
                        <span class="text-muted">School Year</span>
                      </div>
                  </div>

                </div>
                <div class="modal-footer">
                  <button id="delUser" type="button" class="btn btn-default"><i class="fa fa-lock"></i></button>
                  <button id="editUser" type="button" class="btn btn-primary">Edit Information</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- view modal -->

          <!-- edit modal -->
        <div class="modal fade" id="user-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <form id="user-edit-form" class="form-horizontal" role="form">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="edit-user-label"></h4>
                </div>
                <div class="modal-body row">

                  <div class="row col-md-12">
                    <div class="col-md-5">
                      <!-- Your first column here -->
                      <img src="//placehold.it/200/6666ff/fff" class="center-block img-responsive img-circle">
                    </div>

                    <div class="col-md-7">
                      <div class="row">
                        <p id="modal-type" class="lead text-muted">Full Name</p>

                          <div class="col-sm-4">
                            <div class="form-group">
                              <input id="#modal-edit-first" type="text" class="form-control" placeholder="First Name" name="firstname" />
                            </div>
                          </div> 

                          <div class="col-sm-2"> 
                            <div class="form-group">
                              <input id="#modal-edit-mid" type="text" class="form-control" placeholder="Middle Name" name="midname" />
                            </div>
                          </div>

                          <div class="col-sm-4">
                            <div class="form-group">
                              <input id="#modal-edit-last" type="text" class="form-control" placeholder="Last Name" name="lastname" />
                            </div>
                          </div>
                        
                      </div>

                      <div class="row">
                        <div class="col-md-7">
                        <div class="form-group">
                          <span class="lead text-muted">Gender</span><br/><br/>
                          <select class="form-control" name="gender">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                          </select>
                        </div>
                      </div>         
                      </div>

                      <div class="row">
                          <p id="modal-type" class="lead text-muted">Birthday</p>

                          <div class="col-sm-7">
                            <div class="form-group">
                              <input id="#modal-edit-birth" type="date" class="form-control" placeholder="Birthday" name="bday" />
                            </div>
                          </div>        
                      </div>

      
                    </div>

                  </div>

                  <div class="row col-sm-12">
                    <hr />
                        <span class="lead text-muted">Address</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-address" type="text" class="form-control" placeholder="Address" name="address" />
                          </div>
                        </div>
                        <span class="lead text-muted">Email</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-email" type="text" class="form-control" placeholder="Email" name="email" />
                          </div>
                        </div>
                        <span class="lead text-muted">Contact</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-contact" type="text" class="form-control" placeholder="Contact #" name="contact" />
                          </div>
                        </div>
                        <span class="lead text-muted">School Year</span>
                        <div class="form-group">
                          <div class="col-md-12">
                          <select class="form-control" name="sy">
                            <?php include('proc/process-view-cur-select.php'); ?>
                          </select>
                          </div>
                        </div>
                        <span class="lead text-muted">Password</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-password" type="password" class="form-control" placeholder="Password" name="password" />
                          </div>
                        </div>
                  </div>

                <input id="#modal-edit-contact" type="text" class="form-control" name="userid" />
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-success">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <!-- edit modal -->

        <!-- confirm del -->
        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        Warning
                    </div>
                    <div class="modal-body del-body">
                      <p id="confirmMessage">
                        
                      </p>
                    </div>
                    <div class="modal-footer">
                        <button id="confirmDelete" type="button" class="btn btn-primary">Confirm</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
      <!-- confirm del -->

<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

  <script src="js/scripts-manage-students.js"></script>
  </body>
</html>