<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>LMI E-LEARNING</title>
	<meta name="generator" content="Bootply" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	
	<link href="dist/flat/css/bootstrap.min.css" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    
	<!-- custom css -->
	<link href="css/styles.css" rel="stylesheet">

	<!-- plugins -->
	<!-- <link href="css/progressjs.min.css" rel="stylesheet"> -->
	<link href="css/bootstrapValidator.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> -->
	
	<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
	<script src="js/navigation.js"></script>
	
</head>
<body>

	<!-- top navigation -->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
					<a class="navbar-brand brand" href="cpanel.php">LMI E-LEARNING</a>
			</div>
	        <div class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="logout.php"><i class="nav-icons glyphicon glyphicon-off"></i>Logout</a></li>
	          </ul>
	        </div>
	      </div>
	</nav>
	<!-- /top navigation -->

	<div class="side container-fluid">
      <div class="side row row-offcanvas">

      	<!-- side navigation -->
        <div class="col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">
        	<!-- user information -->
            <div class="user-cont row">
              <!-- user image -->
              <img src="image/user/default-user.png" width="50px" height="50px" class="img-responsive img-circle pull-left">
              <!-- user basic information -->
              <div class="user-info">
                <span class="user-info-name">Hello!,<br /> <?php echo $_SESSION['SESS_USER_FULL_NAME']; ?></span>
              </div>
            </div>
			
            <ul class="nav nav-sidebar">
            		<?php
            			$query = mysql_query("SELECT status FROM tbi_status WHERE user_type = '".$_SESSION['SESS_USER_TYPE']."'");
						$res = mysql_fetch_array($query);

            			if($_SESSION['SESS_USER_TYPE'] == 'Student'){
            				echo '<li><a href="cpanel.php"><i class="nav-icons fa fa-home"></i> Home</a></li>';
            				echo '<li><a href="exam.php"><i class="glyphicon glyphicon-chevron-right"></i> Exam</a></li>';
            				echo '<li><a href="view-curriculum.php"><i class="glyphicon glyphicon-chevron-right"></i> View Curriculum</a></li>';
            				echo '<li><a href="uploadfile.php"><i class="glyphicon glyphicon-chevron-right"></i> Upload</a></li>';
            				if($res['status'] == 1)
            				{
            					echo '<li><a href="tbi.php"><i class="glyphicon glyphicon-chevron-right"></i> TBI</a></li>';
            				}
            				echo '<li><a href="calendar.php"><i class="glyphicon glyphicon-chevron-right"></i> Calendar</a></li>';
            				
            				
            			}elseif($_SESSION['SESS_USER_TYPE'] == 'Teacher'){
            				echo '<li><a href="cpanel.php"><i class="nav-icons fa fa-home"></i> Home</a></li>';
            				echo '<li><a href="process-testquestion.php"><i class="glyphicon glyphicon-chevron-right"></i> Add Question</a></li>';
            				echo '<li><a href="uploadfile.php"><i class="glyphicon glyphicon-chevron-right"></i> Upload</a></li>';
            				echo '<li><a href="archive.php"><i class="glyphicon glyphicon-chevron-right"></i> Archived Files</a></li>';
            				if($res['status'] == 1)
            				{
            				echo '<li><a href="tbi-results.php"><i class="glyphicon glyphicon-chevron-right"></i> TBI Results</a></li>';
            				}
            				echo '<li><a href="teacher-add-section.php"><i class="glyphicon glyphicon-chevron-right"></i> Manage Class</a></li>';
            				
            			}else{
            				echo '<li><a href="cpanel.php"><i class="nav-icons fa fa-home"></i> Home</a></li>';
	            			echo '<li><a href="manage-curriculum.php"><i class="glyphicon glyphicon-chevron-right"></i> Manage Curriculum</a></li>';
							echo '<li><a href="manage-teachers.php"><i class="glyphicon glyphicon-chevron-right"></i> Manage Teachers</a></li>';		
							echo '<li><a href="add-teachers.php"><i class="glyphicon glyphicon-chevron-right"></i> New Teacher</a></li>';
	            			echo '<li><a href="manage-school-year.php"><i class="glyphicon glyphicon-chevron-right"></i> Manage School Year</a></li>';
							echo '<li><a href="manage-students.php"><i class="glyphicon glyphicon-chevron-right"></i> Manage Students</a></li>';
            				echo '<li><a href="add-students.php"><i class="glyphicon glyphicon-chevron-right"></i> New Student</a></li>';
            				echo '<li><a href="manage-subjects.php"><i class="glyphicon glyphicon-chevron-right"></i> Manage Subjects</a></li>';
            				echo '<li><a href="manage-section.php"><i class="glyphicon glyphicon-chevron-right"></i> Manage Section</a></li>';
            				echo '<li><a href="manage-tbi.php"><i class="glyphicon glyphicon-chevron-right"></i> Manage TBI</a></li>';
            				echo '<li><a href="process-anouncement.php"><i class="glyphicon glyphicon-chevron-right"></i> Announcement</a></li>';
            				
            			}
            		?>
            	</ul>
            <!-- /side menu items -->

         </div>
         <!-- /side navigation -->

        <div class="col-sm-9 col-md-10 main">
          
          <!--toggle sidebar button-->
          <p class="visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><i class="glyphicon glyphicon-chevron-left"></i></button>
          </p>

          <!--alert messages here //hidden for future use
          <div id="page-alert" class="alert alert-success" data-alert="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <span id="page-alert-message"></span>
          </div>
		   -->