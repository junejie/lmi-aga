<?php
  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<!-- codes starts here -->
<h1><i class="nav-icons fa fa-home"></i> Add Exam<small></small></h1>


     <div class="panel panel-default output">
        <div class="panel-heading">
          <h3>Question Information</h3>
        </div>
        <div class="panel-body">
          <!-- CONTENT BODY HERE -->
          <div class="row">

              <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                   <p><b>Note </b></p>
                   <p>- Field/s with asterix(*) indicates that the field/s are required.</p>
                   <p>- Select an exam first to be able to select an exam set</p>
                   <p>- If SET dropdown has no data it means two things:<br/>
                        1. You didn't select any exam OR<br/>
                        2. The selected exam has no set</p> 
                   <!--<p>- Question field can be multiple. One question per line.</p>-->
                   <p>- Separate each choices per line.</p>
                   <!--<p>- If there's two or more answer just separate it by comma(,).</p>-->
               </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>Exam:</label>
                  <select class="form-control" name="exam">
                    <option></option>
                    <?php include_once('proc/get-exam-name.php'); ?>
                  </select>
                </div>

                <div class="form-group">
                  <div class="set-container">
                      Please select exam first.
                  </div>
                </div>

                <div class="form-group">
                  <label>Situation (If Applicable):</label>
                  <textarea type="text" class="form-control" name="situation" /></textarea>
                </div>
                
                <div class="form-group">
                  <label>Question No:</label>
                  <input type="text" class="form-control" name="question-no" />
                </div>

                <div class="form-group">
                  <label>Question:</label>
                  <input type="text" class="form-control" name="question" />
                </div>

                <!--
                <div class="form-group">
                  <label>Question with image:</label>
                  <input type="text" class="form-control" name="question-img" />
                </div>
                -->

                <div class="form-group">
                  <label>Choices:</label>
                  <textarea type="text" class="form-control" name="choices" /></textarea>
                </div>

                <div class="form-group">
                  <label>Answer:</label>
                  <input type="text" class="form-control" name="answer" />
                </div>

              </div>
            
          </div>
        </div>
        <div class="panel-footer">
            <div class="row">
              <div class="col-md-12">
                <button class="btn btn-primary btn-md pull-right" id="btnSave" data-toggle="tooltip" title="Click to add" data-placement="left">Add</button>          
              </div>
            </div>
        </div>
</div>   


      <!-- MODAL -->
      <div id="modal-alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
          <div class="modal-content text-center">
            <br />
            <img id="loading" src="image/loading.gif" />
            <h1 id="loading-text" class="modal-title">Saving...</h1>
          </div>
        </div>
      </div>
      <!-- MODAL -->


<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

	<script type="text/javascript" src="js/add-exam.js"></script>


  </body>
</html>