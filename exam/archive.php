<?php
    require_once('auth.php');//include for authorization only

    //include config
    require_once('proc/config.php');

    include_once('include/include-head.php');//included links here (head)
?>
<!-- codes starts here -->
<h1><i class="nav-icons fa fa-home"></i>Archived Files <small>Module</small></h1>
<table width="100%">
  <tbody>
    <tr>
      <td>
          <div class="form-group col-sm-6 col-md-6 col-xs-6" style="width:100%">
              <?php
              if($_SESSION['SESS_USER_TYPE'] == 'Teacher')
                    { 
                        $sql = "
                         SELECT filename, filesize, fileloc, filetype, 
                            section, concat(teachers.fname,' ', teachers.midname, ' ',
                             teachers.lname) as teachers_name, teachers.ID as teachers_id,
                             filestatus
                        FROM files 
                        left join teachers on files.teachers_id = teachers.ID";
                        $qry_where = " WHERE filestatus = 'archive' and  teachers_id = '".$_SESSION["SESS_USER_ID"]."'";
                    }
                    else if ($_SESSION['SESS_USER_TYPE'] == 'Student') {
                      # code...
                        $sql = "
                            SELECT filename, filesize, fileloc, filetype, files.section, 
                            concat(teachers.fname,' ', teachers.midname, ' ', teachers.lname) as teachers_name,
                            teachers.ID as teachers_id, 
                            filestatus FROM files left join teachers on files.teachers_id = teachers.ID
                            left join  vw_studentteacher on  teachers.ID = vw_studentteacher.teacher and vw_studentteacher.section = files.section
                            where vw_studentteacher.id ='".$_SESSION["SESS_USER_ID"]."'
                            group by filename
                        ";
                        $qry_where = "  ";
                    }
                ?>
                <table class="table table-hover" width="90%">
                    <tr>
                      <th>File Name</th>
                      <th>File Size</th>
                      <th>Section</th>
                      <th>Teacher</th>
                    </tr>
                <?php
                $qry = mysql_query( $sql." ".$qry_where);
                while ($result = mysql_fetch_array($qry)) {
                  echo '<tr>';
                  echo '<td>'.$result['filename'].'</td>';
                  echo '<td>'.($result['filesize']/1024) . ' KB'.'</td>';
                  echo '<td>'.($result['section']) .'</td>';
                  echo '<td>'.($result['teachers_name']) .'</td>';
                  echo '<td>'.($result['filestatus']) .'</td>';
                  echo '<td>
                    <a href="dl.php?fn='.base64_encode($result['filename']).'" class="btn btn-primary btn-xs">Download</a>
                    <a href="#" onclick="moveToArchive(\''.$result['filename'].'\',\''.($result['section']).'\' ,  \'unarchive\' )"  class="btn btn-primary btn-xs">Unarchive</a>
                    </td>';
                  echo '</tr>';             
                }
                echo '</table>';
              ?>
            </div>
      </td>
    </tr>
  </tbody>
</table>

<!-- codes ends here -->
<?php 
include_once('include/include-body.php');
?>

<script src="js/scripts-manage-students.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
  var inp = document.getElementById('uploader');
      
        $("#uploader").on('change',function(){
            $('.cont-upload').empty();
            var totsize = 0;
            var total = 0;
              for (var i = 0; i < inp.files.length; ++i) {
            var name = inp.files.item(i).name;
            totsize += inp.files.item(i).size;

            var fs = ((inp.files.item(i).size/1024)/1024);
            total += fs;
            //max_size = 51200000; //orig
            max_size = 8388608;
              $('#uploader-cont').append('<p class="alert alert-info"><label style="width:80px;">File Name:</label> '+name+' <span id="'+(i+1)+'" class="pull-right text-success loader_cont"></span><br/><label style="width:80px;">File Size:</label> '+fs.toFixed(2)+' MB</p>')
          }
          if(totsize >= max_size){
            $('#totitem').empty();
            $('#pbmain').fadeOut();
            $("#uploader").val('');
            $('#uploader-cont').empty();
            alert("Allowed Maximum File Size Exceeded");

          }else{
            $('#pbmain').fadeIn();
            $('#percentage').text('0%');
            $('#totitem').html('<strong>Total Size: '+total.toFixed(2)+' MB</strong>');
          }
          
          });
})

function moveToArchive(file,teacher,status){
        $.ajax({
            url :'proc/process-move-archive.php',
            type:'POST',
            data:{"file":file,"teacher":teacher, "status":status}

        }).success(function(msg ){
            if (msg == 'done') {
                alert("Operation Successful.");
            } else{
                alert("Operation Failed.");
            };
            window.location='archive.php';
        })
    }
</script>