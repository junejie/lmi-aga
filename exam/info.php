
<?php include_once('header.php'); ?>
<?php include_once('navigation.php'); ?>
<?php include_once('sqlconnect.php'); ?>
<div class="container">

<div class="col-lg-10 col-md-offset-1">
    <div class="panel panel-default">
        <div class="panel-body">
            <!--<ul class="nav nav-pills nav-wizard">
                  <li class="active"><a href="info.php">Data Information</a><div class="nav-arrow"></div></li>
                  <li><div class="nav-wedge disabled"></div><a href="profile.php" class="nav-item">Profile</a><div class="nav-arrow"></div></li>
                  
                  <li><div class="nav-wedge disabled"></div><a href="#" class="nav-item">Well Done!</a></li>
                </ul>-->

                <div id="reg-items" class="row"><!-- Registration Items -->

                  <div id="data-info"><!-- Item 1 -->
                      <div class="col-md-12">
                        <h3>INFORMATION</h3>
                        <div class="alert alert-info" role="alert">
                            <p><b>Note: </b></p>
                            <p>- Fill up the form to proceed to your exam</p>

                        </div>
                      </div>
                      <div class="col-md-12">

                        <div class="form-group" id="name-group">
                          <label>Fullname: <b class="text-danger">(Required)</b></label>
                          
                          <input type="text" class="form-control" name="name" id="txtname" style="text-transform:uppercase;" />
                        </div>
                        

                        <div class="form-group" id="gender-group">
                          <label>Gender: <b class="text-danger">(Required)</b></label>
                      
                          <select class="form-control" id="cmbgender">
                            <option></option>
                            <option value="MALE">MALE</option>
                            <option value="FEMALE">FEMALE</option>
                          </select>
                        </div>

                        <div class="form-group" id="age-group">
                          <label>Age: <b class="text-danger">(Required)</b></label>
                          
                          <input type="text" maxlength="3" class="form-control" onkeypress="return isNumberKey(event)" name="age" id="txtage" />
                        </div>
                        
                        <div class="form-group" id="dot-group" style="display:none;">
                          <label>Date of Test: </label>
                          <input type="text" class="form-control" readonly name="dateoftest" value="<?php echo date('m-d-Y h:i:A') ?>" id="txtdot" />
                        </div>
                        
                        <div class="form-group" id="educatt-group">
                          <label>Educational Attaintment: <b class="text-danger">(Required)</b></label>
        
                          <textarea class="form-control" name="educatt" id="txteducatt" style="text-transform:uppercase;"></textarea>
                        </div>

                        <div class="form-group" id="curremp-group">
                          <label>Current Employer(if applicable):</label>
                          <input type="email" class="form-control" name="curremp" id="txtcurremp" style="text-transform:uppercase;" />
                        </div>

                        <div class="form-group" id="currpos-group">
                          <label>Current Position(if applicable):</label>
                          <input type="email" class="form-control" name="currpos" id="txtcurrpos" style="text-transform:uppercase;" />
                        </div>

                        <div class="form-group" id="level-group">
                          <label>Level: <b class="text-danger">(Required)</b></label>
                          <select class="form-control" id="cmblevel">
                            <option></option>
                            <?php 
                              $qry = mysql_query("SELECT * FROM level");
                              while ($result=mysql_fetch_array($qry)) {
                                echo '<option value="'.$result['id'].'">'.$result['level'].'</option>';
                              }
                            ?>
                          </select>

                        </div>

                        <div class="text-danger" align="center" id="val"></div>

                      </div>
                  </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
          <div class="col-md-12">
            <button class="btn btn-primary pull-right btn-sm" onClick="profiling()" id="btnNext" data-toggle="tooltip" title="Click to proceed">Next</button>          
          </div>
        </div>
    </div>
</div>


</div>

<script type="text/javascript" src="js/profile.js"></script>
<?php include_once('footer.php'); ?>



