<?php

  require_once('auth.php');

  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
     <div class="panel panel-default output">
        <div class="panel-heading">
            <h3></h3>
        </div>
        <br/>
          <div class="panel-body">
            <!-- CONTENT BODY HERE -->
            <div class="row">
                
                <div class="col-md-12">
                <div class="pull-right">
                    <button class="btn btn-primary btn-lg" id="btnaddinstruction" 
                    onClick="showannouncement()">Add Announcement</button>
                </div>
                <div  id="savealert" class="alert alert-success" align="center" style="height:200px;position:fixed;z-index:2;left:40%;right:40%;top:10%;display:none;">
                    <h1 id="savetext" style="position:absolute;top:35%;left:50%;transform: translateX(-50%) translateY(-35%);"></h1>
                </div>
                  <br/> <br/> <br/>
                    <div id="light" class="white_content">
                        <p align="right">
                            <a href = "javascript:void(0)" 
                            onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">X close</a>
                        </p>
                        <div id="editquest-cont">
                        </div>
                    </div>
                  <div id="fade" class="black_overlay"></div>
                  <div class="datalist" style="height:300px;overflow:scroll;">
                    <?php
                    $id2 = $_SESSION['SESS_USER_ID'];
                    $qry = mysql_query("SELECT * FROM announcement");
                    ?>
                    <table class="table table-striped table-hover" 
                    style="width:100%;">
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Announce To</th>
                            <th></th>
                        </tr>
                    <?php

                    while ($result= mysql_fetch_array($qry)) {
                      echo '<tr>
                        <td>'.$result['id'].'</td>
                        <td>'.$result['title'].'</td>
                        <td>'.$result['date'].'</td>
                        <td>'.$result['announceto'].'</td>
                        <td><button value="Delete" onclick=" deleteAnnouncement(\''.$result['id'].'\') ">Delete</button></td>
                        </tr>';
                    }
                    echo '</table>';
                    ?>
                  </div>
                 </div>
                    <div class="col-md-12">
                    <hr/>
                    <div id="announcement_cont">
                    </div>
                        <div id="subset-cont" style="display:none;">
                        <div class="form-group" id="level-group">
                        <label>Subject: <b class="text-danger">(Required)</b></label>
                        <?php
                        $subject = mysql_query("SELECT * FROM subject");
                        echo '<select id="qsubsetid" class="form-control">
                        <option value=""></option>';
                        while ($resultsubj = mysql_fetch_array($subject)) {
                        echo '<option value="'.$resultsubj['subjectid'].'">'.$resultsubj['subjectname'].'</option>';
                        }
                        echo '</select>';
                        ?>
                        </div>
                        <div class="form-group" id="level-group">
                            <label>Question Type: <b class="text-danger">(Required)</b></label>
                            <select id="qtype" class="form-control" onChange="showmore(this.value)">
                                <option></option>
                                <option value="multiplec">MULTIPLE CHOICE</option>
                            </select>
                        </div>
                        <div class="hiddenobject"> </div>
                        </div>
                    </div>
          </div>
        </div>

      <!-- MODAL -->
        <div id="modal-alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content text-center">
                    <br />
                    <img id="loading" src="image/loading.gif" />
                    <h1 id="loading-text" class="modal-title">Saving...</h1>
                </div>
            </div>
        </div>
      <!-- MODAL -->
<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>
<script src="js/navigation.js"></script>
<script src="js/annoucement.js"></script>