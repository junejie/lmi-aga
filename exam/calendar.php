<?php
  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>


<link href='./js/calendar/fullcalendar.css' rel='stylesheet' />
<link href='./js/ui/ui/ui.css' rel='stylesheet' />
<link href='./js/calendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='./js/calendar/lib/moment.min.js'></script>
<script src='./js/calendar/lib/jquery.min.js'></script>
<script src='./js/calendar/fullcalendar.min.js'></script>
<script src='./js/ui/ui/jquery-ui.js'></script>
<script>

  $(document).ready(function() {
  
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      defaultDate: moment().format("YYYY-MM-DD"),
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: {
        url: 'proc/process-view-calendar.php',        
        error: function() {
          $('#script-warning').show();
        }
      },
      eventClick: function(event) {
            if (event.url) {
                window.open(event.url);
                return false;
            }else{

                $.ajax({
                    url :'proc/process-view-data-calendar.php?title='+ event.title+ '&start=' + event.start._i,
                    type:'GET',                  
                    success : function(data){

                        $('#eventMsg').html( data );
                        $('#eventMsg').dialog();
                    }
                })
            }
        },
      loading: function(bool) {
        $('#loading').toggle(bool);
      }
    });
    
  });

</script>
<style> 
  #script-warning {
    display: none;
    background: #eee;
    border-bottom: 1px solid #ddd;
    padding: 0 10px;
    line-height: 40px;
    text-align: center;
    font-weight: bold;
    font-size: 12px;
    color: red;
  }

  #loading {
    display: none;
    position: absolute;
    top: 10px;
    right: 10px;
  }

  #calendar {
    max-width: 900px;
    margin: 40px auto;
    padding: 0 10px;
  }

</style>
  <div id='script-warning'>
    <code>proc/process-view-calendar.php</code> must be running.
  </div>

  <div id='loading'>loading...</div>

  <div id='calendar'></div>
  <div id="eventMsg"></div>