<?php
  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<!-- codes starts here -->



     <div class="panel panel-default output">
        <div class="panel-heading">
          <h3>Type of Test</h3>
        </div>
        <br/>
        
        
          <div class="panel-body">
            <!-- CONTENT BODY HERE -->
            <div class="row">

                

                <div class="col-md-12">
                  

                  
                  <div class="datalist">
                    

                  </div>
              
            </div>
          </div>
          
        </div>
        


      <!-- MODAL -->
      <div id="modal-alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
          <div class="modal-content text-center">
            <br />
            <img id="loading" src="image/loading.gif" />
            <h1 id="loading-text" class="modal-title">Saving...</h1>
          </div>
        </div>
      </div>
      <!-- MODAL -->


<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

	<script type="text/javascript" src="js/typeoftest.js"></script>


  </body>
</html>