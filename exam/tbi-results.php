<?php
require_once('auth.php');
require_once('proc/config.php');  
include_once('include/include-head.php');



	$teacherid = $_SESSION['SESS_USER_ID'];
	$q = mysql_query("

select 
	teacher,
	subject2,
	teachers_class.section as section2,
	sum,count , avg
from 
	(SELECT 
		teachers_class.teacher, 
		teachers_class.subject as subject2,
		teachers_class.section
	FROM `teachers_class`
	where teacher ='$teacherid'
)as teachers_class
left join  (
	SELECT 
	evaluationresult.subject as subject,
	evaluationresult.teacherid,
	students.section as section,
	sum(evaluationresult.answer) as sum,
	count(evaluationresult.answer) as count,
	avg(evaluationresult.answer) as avg
	FROM `evaluationresult`
	left join students on (
	 students.id = evaluationresult.studentid
	)
		where evaluationresult.teacherid = '$teacherid'
		group by evaluationresult.subject, students.section
) as evaluationresult 
on  (
	subject2 = evaluationresult.subject
	and teachers_class.section  = evaluationresult.section
	)
;");
?>

<h1><i class="nav-icons fa fa-home"></i>TBI Results <small>Module</small></h1>
	<div class="table-responsive">
	    <table class="table table-condensed">	
	      <thead>
	        <tr>
	          <th>Section</th>
	          <th>Subject Name</th>
	          <th>Count</th>
	          <th>Average</th>
	        </tr>
	      </thead>
	      <tbody id="tableResult">
	      		<?php
	      			while ($d = mysql_fetch_array($q)) {
						echo '<tr>
							<td>'. $d['section2'] .'</td>
							<td>'. $d['subject2'] .'</td>
							<td>'. $d['count'] .'</td>
							<td>'. ($d['avg'])/5*100 .' %</td>
						</tr>';
					}
	      		?>
	       </tbody>
	    </table>
    </div>
</html>