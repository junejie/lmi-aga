<?php
  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<!-- codes starts here -->
<h1><i class="nav-icons fa fa-home"></i> Add Batteries<small></small></h1>


     <div class="panel panel-default output">
        <div class="panel-heading">
          <h3>Exam Information</h3>
        </div>
        <br/>
        <div class="form-group" align="center">
          <p>
            <button class="btn btn-primary btn-lg" onClick="showhidden()">Click to add exam subset</button>
          </p>
        </div>

        <div id="hides" style="display:none;">
          <div class="panel-body">
            <!-- CONTENT BODY HERE -->
            <div class="row">

                <div class="col-md-12">
                  <div class="alert alert-info" role="alert">
                       <p><b>Note </b></p>
                       <p>- SET can be multiple, just add comma(,) for each set.</p> 
                       <p>- The first SET will be the first one typed.</p>
                       <p>- If there's no SET just leave it blank.</p>
                  </div>
                </div>
                
                <div class="col-md-6">
                  <div class="form-group err" id="tot-group">
                    <label>Type of Test:</label>
                    <div id="typeoftest">
                    </div>
                   
                  </div>

                  <div class="form-group err" id="subset-group">
                    <label>Subsetname:</label>
                    <input type="text" class="form-control uppercase" id="subsetname" >
                    
                  </div>

                  <div class="form-group err" id="time-group">
                    <label>Time Limit (mins.):</label>
                    <input maxlength="3" type="text" class="form-control" id="timelimit" onkeypress="return isNumberKey(event)" style="width:10%;" >
                    
                  </div>

                  <div class="form-group err" id="toa-group">
                    <label>Type of Answer:</label>
                    <select class="form-control uppercase" id="toa" >
                      <option></option>
                      <option value="MULTIPLE CHOICE">MULTIPLE CHOICE</option>
                      <option value="FILL-IN THE BLANK">FILL-IN THE BLANK</option>
                      <option value="AGREE/DISAGREE">AGREE/DISAGREE</option>
                    </select>
                  </div>
                  <div class="validation text-danger" align="center"></div>
                  <h3 class="text-info" align="center">Recently Added Subset</h3>
                  <hr/>
                  <div class="datalist" style="height:250px;overflow:scroll;overflow-x:hidden;">
                    

                  </div>
                  
            </div>

            
           
          </div>
          <div class="panel-footer">
              <div class="row">
                <div class="col-md-12">
                  <button onClick="addexamsubset()" class="btn btn-primary btn-md pull-right" id="btnSave" data-toggle="tooltip" title="Click to add" data-placement="left">Add</button>          
                </div>
              </div>
          </div>
        </div>
      </div>   


      <!-- MODAL -->
      <div id="modal-alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
          <div class="modal-content text-center">
            <br />
            <img id="loading" src="image/loading.gif" />
            <h1 id="loading-text" class="modal-title">Saving...</h1>
          </div>
        </div>
      </div>
      <!-- MODAL -->


<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

	<script type="text/javascript" src="js/add-examsubset.js"></script>


  </body>
</html>