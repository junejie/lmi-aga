<?php

  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
?>
<!-- Large modal -->

<style type="text/css">
.borderless tbody tr td, .borderless tbody tr th, .borderless thead tr th {
    border: none;
}

</style>

<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title">Evaluation Form</p>
      </div>
      <div class="modal-body">
        <?php

        $tid = $_GET['teachersid'];
        $subject = $_GET['subject'];
        $qry = mysql_query( 
            sprintf("SELECT * FROM teachers 
                WHERE ID='%s'; ",mysql_real_escape_string($tid) )
            );
        $result =mysql_fetch_array($qry);
       
            ?>
            <table width="100%">
              <tbody>
                <th width="50%" style="vertical-align:top;"><p><label class="label-control">
            <?php echo $result['fname'] .' '. $result['lname']; ?></label><br/>
            <label class="label-control"><?php echo $result['addr'];?></label><br/>
            <label class="label-control"><?php echo $result['contact'];?></label>
            </p></th>
                <th width="50%" style="vertical-align:top;">
                  <ul style="list-style-type: none;">
                    <li>Rating:</li>
                    <li style="padding-left:30px;">5 - Excellent</li>
                    <li style="padding-left:30px;">4 - Very Good</li>
                    <li style="padding-left:30px;">3 - Average</li>
                    <li style="padding-left:30px;">2 - Fair</li>
                    <li style="padding-left:30px;">1 - Poor</li>
                  </ul>
                  </th>
              </tbody>
            </table>  
            
            <hr>
        <p>
            <b>Clarity:  teaching behaviours that serve to explain or clarify concepts and principles</b>
        </p>

        <!-- GETTING TBI -->
<?php
    $qry = sprintf("SELECT *
            FROM  tbi_questions;"  
    );
    $qry = mysql_query( $qry ); 
    $counter = 1;   
    while ($result= mysql_fetch_array($qry)) {
        echo '<p>'.$result['tbi_question'].'</p>';
        ?>
        <p>
          <input type="radio" value="5" name="clarity<?php echo $counter;?>" /> 5 <br/>
          <input type="radio" value="4" name="clarity<?php echo $counter;?>" /> 4 <br/>
          <input type="radio" value="3" name="clarity<?php echo $counter;?>" /> 3 <br/>
          <input type="radio" value="2" name="clarity<?php echo $counter;?>" /> 2 <br/>
          <input type="radio" value="1" name="clarity<?php echo $counter;?>" /> 1 <br/>
        </p>
        <?php
        $counter++;
    }
    ?>
        <button class="btn btn-primary btn-md" 
        onclick="getradio( <?php echo "'".$tid."', '".$subject."'"; ?> )">
        Submit</button>
      </div>
    </div>
  </div>
</div>

