<?php 
  session_start();
  session_destroy();
?>
<?php include_once('header.php'); ?>
<?php include_once('navigation.php'); ?>
<?php include_once('sqlconnect.php'); ?>
<div class="container">

<div class="col-lg-10 col-md-offset-1">
    <div class="panel panel-default">
        <div class="panel-body">
            <h1 class="text-success" align="center"><span class="glyphicon glyphicon-ok"></span> Thanks for taking the exam</h1>
            <p align="center"><button class="btn btn-primary btn-lg" onClick="self.close()">CLICK HERE TO CLOSE THIS WINDOW</button></p>
        </div>

    </div>
</div>

<script type="text/javascript" src="js/profile.js"></script>
<?php include_once('footer.php'); ?>



