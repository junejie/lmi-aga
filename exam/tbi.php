<?php

  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<h1><i class="nav-icons fa fa-home"></i>Teacher's Behavioral Inventory</h1>
     <div class="panel panel-default output">
        <div class="panel-heading">
          <h3></h3>
        </div>
        <br/>
       
          
          <div class="panel-body">
            <!-- CONTENT BODY HERE -->
            <div class="row">
                
                <div class="col-md-12">
                  <div  id="savealert" class="alert alert-success" align="center" style="height:200px;position:fixed;z-index:2;left:40%;right:40%;top:10%;display:none;">
                    <h1 id="savetext" style="position:absolute;top:35%;left:50%;transform: translateX(-50%) translateY(-35%);"></h1>
                  </div>
                  <?php
                    
                  ?>

                    <div id="light" class="white_content">
                      <p align="right">
                        <a href = "javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">X close</a>
                      </p>
                      <div id="editquest-cont">
                      </div>
                    </div>
                  <div id="fade" class="black_overlay"></div>

                  <div class="datalist" style="height:300px;overflow:scroll;">


    <?php
    $id2 = $_SESSION['SESS_USER_ID'];
    echo $id2;
    $qry = sprintf("
        SELECT 
        concat(teachers.fname, ' ',teachers.midname,' ', teachers.lname) as t_name ,
        takenevaluation.status as `status`,
        teachers_class.subject,
        teachers.id as teachers_id
        FROM students
            left join teachers_class on students.section = teachers_class.section
            left join teachers on teachers.ID = teachers_class.teacher
            left join takenevaluation on (
            takenevaluation.teachersid = teachers.ID 
            and takenevaluation.subject = teachers_class.subject 
            )
        WHERE students.ID = '$id2' AND teachers_class.teacher !=  '';"
    );
    //echo $qry;

    $qry = mysql_query( $qry );


    echo '<table class="table table-striped table-hover" style="width:700px;">
            <tr>
            <th>Teacher ID</th>
            <th>Teacher Name</th>
            <th>Subject</th>
            <th>Status</th>
            <th>Evaluate</th>
            </tr>
        ';
    while ($result= mysql_fetch_array($qry)) {
 
      echo '<tr>
            <td>'.$result['teachers_id'].'</td>
            <td>'.$result['t_name'].'</td>
            <td>'.$result['subject'].'</td>
            
            <td>'.($result['status']  == 'FINISHED' ? "FINISHED":"UNFINISHED").'</td>';
    ?>
            <td>
                <button <?php echo ( $result['status'] == 'FINISHED' ? 'disabled': "" ); ?>
                    onclick="showTBIModal( <?php echo "'".$result['teachers_id']."','".$result['subject']."'" ;?> )">Show.</button>
                
            </td>
         </tr>
    <?php
    }
    echo '</table>';
    ?>

<!-- container of modal -->
<div id="mainModal"></div>

<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

<script type="text/javascript">
    function showTBIModal(teachersid,subject){
        $.ajax({
          type: 'GET',
          url: 'modal-dynamic-tbi.php?teachersid='+teachersid+'&subject='+subject,
          success: function(msg){
            console.log(teachersid)
            $('#mainModal').html(msg);
            $('#mymodal').modal('show');
          }
        });
    }

    function getradio(teacherid, subject){
        console.log('teacherid',teacherid)
        var value = '';
        var error = 0;
        var fieldscount = $('.modal-body INPUT').length/5;
        for (var i = 1; i <= fieldscount ; i++) {
            if($('input[name=clarity'+i+']:checked').val() == '' || $('input[name=clarity'+i+']:checked').val() == undefined){
            error += 1;
            }else{
            value += $('input[name=clarity'+i+']:checked').val() + ';';
            }
        }
        if(error > 0){
            alert('Please complete the evaluation!');
        }else{
            datastring = 'value='+ value + '&teachersid=' + teacherid+'&subject='+subject;
            $.ajax({
                type: 'POST',
                url: 'proc/evaluation.php',
                data: datastring,
                success: function(msg){
                    //window.location = 'cpanel.php';
                    alert("Saved");
                    $("#mymodal").hide();
                    $(".modal-backdrop").hide();
                }
            })
        }
    }
</script>
