<?php

  //include auth
  require_once('auth.php');//include for authorization only
   
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<!-- codes starts here -->
<h1><i class="nav-icons fa fa-home"></i>Upload <small>Module</small></h1>

<?php 
if($_SESSION['SESS_USER_TYPE'] == 'Teacher')
{ 
  ?>
<form enctype="multipart/form-data" class="form-horizontal" action="upload-custom.php" method="post">
    <div class="form-group col-sm-2 col-lg-2 col-md-2 col-xs-2" style="width:100%;">     
    <table cellpadding="10" height="130px">
        <tbody>
          <tr><td><input name="myfile[]" id="uploader" style="border:1px solid #5983FF;" class="btn btn-link btn-sm" multiple type="file" /></td></tr>
          <tr><td><select id="idsection" name="section" style="width:100%;" class="required">
              <option>-- Select Section --</option>
              <?php 
                $qry_section = mysql_query("SELECT sectionname FROM section");
                while ($result_section = mysql_fetch_array($qry_section)) {
                  echo '<option value="'.$result_section['sectionname'].'">'.$result_section['sectionname'].'</option>';
                 
                }
              ?>
          </select></td></tr>
          <tr><td><button class="btn btn-primary btn-sm pull-right" id="up" onclick="upload('frmupload')">Upload File</button></td></tr>
          <tr><td><p class="text-danger" align="center"><b>Max Total File Size: 25MB</b></p></td></tr>
          <tr><td><p class="text-success" align="center" id="totitem"></p>
        <div class="progress progress-striped active" id="pbmain" style="height:20px;display:none"> 
        <div id="pbcontent" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"> 
          <span class="label"><span id="percentage" style="font-size:12pt;color:#000">0/0</span></span> 
        </div></div>
      <span id="uploader-cont" class="cont-upload">
        </span> </td></tr>
        </tbody>
    </table>
    </div>
</form>
<?php 
}
?>
<table width="100%">
  <tbody>
    <tr>
      <td>
          <div class="form-group col-sm-6 col-md-6 col-xs-6" style="width:100%">
  <?php
  if($_SESSION['SESS_USER_TYPE'] == 'Teacher')
    { 
        $sql = "
         SELECT filename, filesize, fileloc, filetype, 
            section, concat(teachers.fname,' ', teachers.midname, ' ',
             teachers.lname) as teachers_name, teachers.ID as teachers_id,
             filestatus
        FROM files 
        left join teachers on files.teachers_id = teachers.ID";
        $qry_where = " WHERE  filestatus = 'unarchive' and  teachers_id = '".$_SESSION["SESS_USER_ID"]."'";
    }
    else if ($_SESSION['SESS_USER_TYPE'] == 'Student') {
      # code...
        $sql = "
            SELECT filename, filesize, fileloc, filetype, files.section, 
            concat(teachers.fname,' ', teachers.midname, ' ', teachers.lname) as teachers_name,
            teachers.ID as teachers_id, 
            '' as filestatus FROM files left join teachers on files.teachers_id = teachers.ID
            left join  vw_studentteacher on  teachers.ID = vw_studentteacher.teacher and vw_studentteacher.section = files.section
            where vw_studentteacher.id ='".$_SESSION["SESS_USER_ID"]."'
            group by filename
        ";
        $qry_where = "  ";
    }
    ?>
    <hr/><h2>Uploaded Files</h2><hr/>
    <table class="table table-hover" width="90%">
        <tr>
          <th>File Name</th>
          <th>File Size</th>
          <th>Section</th>
          <th>Teacher</th>
        </tr>
    <?php
        $qry = mysql_query( $sql." ".$qry_where);
        while ($result = mysql_fetch_array($qry)) {
            echo '<tr>';
            echo '<td>'.$result['filename'].'</td>';
            echo '<td>'.($result['filesize']/1024) . ' KB'.'</td>';
            echo '<td>'.($result['section']) .'</td>';
            echo '<td>'.($result['teachers_name']) .'</td>';
            echo '<td>'.($result['filestatus']) .'</td>';
            echo '<td>
            <a href="dl.php?fn='.base64_encode($result['filename']).'" class="btn btn-primary btn-xs">Download</a>';
            if($_SESSION['SESS_USER_TYPE'] == 'Teacher') {
                echo '<a href="#" onclick="moveToArchive(\''.$result['filename'].'\',\''.($result['section']).'\'  ,\'archive\')"  class="btn btn-primary btn-xs">Archive</a>';
            } 
            echo '</td>';
            echo '</tr>';             
        }
    echo '</table>';
  ?>
</div>
      </td>
    </tr>
  </tbody>
</table>

<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

  <script src="js/scripts-manage-students.js"></script>
  <script type="text/javascript" src="js/jquery.form.js"></script>
  </body>
</html>
<script type="text/javascript">
$(document).ready(function(){
  var inp = document.getElementById('uploader');
      
        $("#uploader").on('change',function(){
            $('.cont-upload').empty();
            var totsize = 0;
            var total = 0;
              for (var i = 0; i < inp.files.length; ++i) {
            var name = inp.files.item(i).name;
            totsize += inp.files.item(i).size;

            var fs = ((inp.files.item(i).size/1024)/1024);
            total += fs;
            //max_size = 51200000; //orig
            max_size = 8388608;
              $('#uploader-cont').append('<p class="alert alert-info"><label style="width:80px;">File Name:</label> '+name+' <span id="'+(i+1)+'" class="pull-right text-success loader_cont"></span><br/><label style="width:80px;">File Size:</label> '+fs.toFixed(2)+' MB</p>')
          }
          if(totsize >= max_size){
            $('#totitem').empty();
            $('#pbmain').fadeOut();
            $("#uploader").val('');
            $('#uploader-cont').empty();
            alert("Allowed Maximum File Size Exceeded");

          }else{
            $('#pbmain').fadeIn();
            $('#percentage').text('0%');
            $('#totitem').html('<strong>Total Size: '+total.toFixed(2)+' MB</strong>');
          }
          
          });
    });

    function moveToArchive(file,teacher,status){
        $.ajax({
            url :'proc/process-move-archive.php',
            type:'POST',
            data:{"file":file,"teacher":teacher,"status":status}

        }).success(function(msg ){
            if (msg == 'done') {
                alert("Operation is Successful.");
            } else{
                alert("Operation Failed.");
            };
            window.location='uploadfile.php';
        })
    }
//uploader multiple
function upload(formid){

    var formElement = document.getElementById(formid);
    var myControls = document.getElementById('uploader');
    if($("#uploader").val() != "" && $("#idsection").val() != '-- Select Section --'){
     //alert(myControls.files.length)
        $('form').ajaxForm({
          beforeSend: function(){
          },

          uploadProgress: function(event, position, total, percentComplete) {
            
            var percentVal = percentComplete + '%';
            $('#pbcontent').attr('style','width:'+percentVal+'%;');
            $('#percentage').text(percentVal);
          },
          success: function() {
              var percentVal = '100%';
              $('#pbcontent').attr('style','width:'+percentVal+'%;');
              $('#percentage').text(percentVal);
        },
        complete: function(xhr) {
          alert("Upload Completed");
          window.location='uploadfile.php';
        }

        });
    

    }
    else if($("#uploader").val() == '')  {
      alert("Please select a file first!");
      $("#uploader").focus();
    }
    else if($("#idsection").val() == '-- Select Section --')  {
      alert("Please select a section first!");
      $("#idsection").focus();
    }
    else{
      alert("Please select a file first!");
    }

    }
</script>