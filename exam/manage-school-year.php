<?php
  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<!-- codes starts here -->
<h1><i class="nav-icons fa fa-home"></i>School Year <small>Module</small></h1>
	<button id="addData" type="button" class="btn btn-success">Add School Year</button>
	<div class="table-responsive">
	    <table class="table table-condensed">
	      <thead>
	        <tr>
	          <th>ID</th>
	          <th>School Year</th>
	          <th>Curriculum</th>
	          <th>Manage</th>
	        </tr>
	      </thead>
	      <tbody id="tableResult">
	        <!-- insert members -->
	        <?php include_once('proc/process-view-sy.php') ?>
	      </tbody>
	    </table>
    </div>

    <!-- view modal -->
          <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="userModalLabel">Name Here</h4>
                </div>
                <div class="modal-body row">

                  <div class="row col-md-12">
                      <div class="col-md-6">
                        <h4 id="modal-address">Subject Name</h4>
                        <span class="text-muted">School Year</span>
                        <input type="hidden" name="userid">
                      </div>

                      <div class="col-md-6">
                        <h4 id="modal-email">Description</h4>
                        <span class="text-muted">Curriculum</span>
                      </div>
                  </div>

                </div>
                <div class="modal-footer">
                  <button id="delUser" type="button" class="btn btn-danger">Delete</button>
                  <button id="editUser" type="button" class="btn btn-primary">Edit Information</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- view modal -->

          <!-- edit modal -->
        <div class="modal fade" id="user-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <form id="user-edit-form" class="form-horizontal" role="form">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="edit-user-label"></h4>
                </div>
                <div class="modal-body row">
                        <span class="lead text-muted">School Year:</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-email" type="text" class="form-control" placeholder="Subject Name" name="email" />
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12">
                          <span class="lead text-muted">Curriculum</span><br/><br/>
                          <select class="form-control" name="address">
                            <?php include('proc/process-view-cur-select.php'); ?>
                          </select>
                          </div>
                        </div>
                        <input type="text" name="userid">
                  </div>

                <div class="modal-footer">
                  <button type="submit" class="btn btn-success">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <!-- edit modal -->

          <!-- add modal -->
        <div class="modal fade" id="user-add-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <form id="user-add-form" class="form-horizontal" role="form">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="edit-user-label">New Subject</h4>
                </div>
                <div class="modal-body row">
                        <span class="lead text-muted">School Year Name:</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-email" type="text" class="form-control" placeholder="Subject Name" name="email" />
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12">
                          <span class="lead text-muted">Curriculum</span><br/><br/>
                          <select class="form-control" name="address">
                            <?php include('proc/process-view-cur-select.php'); ?>
                          </select>
                          </div>
                        </div>
                  </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-success">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <!-- add modal -->

        <!-- confirm del -->
	      <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	          <div class="modal-dialog">
	              <div class="modal-content">
	                  <div class="modal-header">
	                      Warning
	                  </div>
	                  <div class="modal-body del-body">
	                  	<p id="confirmMessage">
	                      Are you sure you want to delete this item?
	                    </p>
	                  </div>
	                  <div class="modal-footer">
	                      <button id="confirmDelete" type="button" class="btn btn-danger">Confirm</button>
	                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                  </div>
	              </div>
	          </div>
	      </div>
      <!-- confirm del -->

<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

  <script src="js/scripts-manage-sy.js"></script>
  </body>
</html>