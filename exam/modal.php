<!-- Large modal -->

<style type="text/css">
.borderless tbody tr td, .borderless tbody tr th, .borderless thead tr th {
    border: none;
}

</style>

<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title">Evaluation Form</p>
      </div>
      <div class="modal-body">
        <?php

        $teacherid = $_SESSION['teacher'];

        $qry = mysql_query("SELECT * FROM teachers WHERE ID='$teacherid'");
        $result =mysql_fetch_array($qry);
       
        echo '<p><label class="label-control">'.$result['fname'] .' '. $result['lname'].'</label><br/>
                                  <label class="label-control">'.$result['addr'].'</label><br/>
                                  <label class="label-control">'.$result['contact'].'</label>

              </p>


              <hr>';
        ?>
          
        <p><b>Clarity:  teaching behaviours that serve to explain or clarify concepts and principles</b></p>
        

        <!-- GETTING TBI -->
        
        <p>1.  gives several examples of each concept</p>
        <p>
          <input type="radio" value="5" name="clarity1" /> 5 <br/>
          <input type="radio" value="4" name="clarity1" /> 4 <br/>
          <input type="radio" value="3" name="clarity1" /> 3 <br/>
          <input type="radio" value="2" name="clarity1" /> 2 <br/>
          <input type="radio" value="1" name="clarity1" /> 1 <br/>
        </p>

        <p>2.  uses concrete, everyday examples to explain concepts and principles</p>
        <p>
          <input type="radio" value="5" name="clarity2" /> 5 <br/>
          <input type="radio" value="4" name="clarity2" /> 4 <br/>
          <input type="radio" value="3" name="clarity2" /> 3 <br/>
          <input type="radio" value="2" name="clarity2" /> 2 <br/>
          <input type="radio" value="1" name="clarity2" /> 1 <br/>
        </p>

        <p>3.  fails to define new or unfamiliar terms</p>
        <p>
          <input type="radio" value="5" name="clarity3" /> 5 <br/>
          <input type="radio" value="4" name="clarity3" /> 4 <br/>
          <input type="radio" value="3" name="clarity3" /> 3 <br/>
          <input type="radio" value="2" name="clarity3" /> 2 <br/>
          <input type="radio" value="1" name="clarity3" /> 1 <br/>
        </p>

        <p>4.  uses graphs and diagrams to facilitate explanation</p>
        <p>
          <input type="radio" value="5" name="clarity4" /> 5 <br/>
          <input type="radio" value="4" name="clarity4" /> 4 <br/>
          <input type="radio" value="3" name="clarity4" /> 3 <br/>
          <input type="radio" value="2" name="clarity4" /> 2 <br/>
          <input type="radio" value="1" name="clarity4" /> 1 <br/>
        </p>

        <p>5.  repeats difficult ideas several times</p>
        <p>
          <input type="radio" value="5" name="clarity5" /> 5 <br/>
          <input type="radio" value="4" name="clarity5" /> 4 <br/>
          <input type="radio" value="3" name="clarity5" /> 3 <br/>
          <input type="radio" value="2" name="clarity5" /> 2 <br/>
          <input type="radio" value="1" name="clarity5" /> 1 <br/>
        </p>

        <p>6.  stresses most important points by pausing, speaking slowly, raising voice, etc.</p>
        <p>
          <input type="radio" value="5" name="clarity6" /> 5 <br/>
          <input type="radio" value="4" name="clarity6" /> 4 <br/>
          <input type="radio" value="3" name="clarity6" /> 3 <br/>
          <input type="radio" value="2" name="clarity6" /> 2 <br/>
          <input type="radio" value="1" name="clarity6" /> 1 <br/>
        </p>

        <p>7.  answers students’ questions thoroughly</p>
        <p>
          <input type="radio" value="5" name="clarity7" /> 5 <br/>
          <input type="radio" value="4" name="clarity7" /> 4 <br/>
          <input type="radio" value="3" name="clarity7" /> 3 <br/>
          <input type="radio" value="2" name="clarity7" /> 2 <br/>
          <input type="radio" value="1" name="clarity7" /> 1 <br/>
        </p>

        <p>8.  suggests ways of memorizing complicated ideas</p>
        <p>
          <input type="radio" value="5" name="clarity8" /> 5 <br/>
          <input type="radio" value="4" name="clarity8" /> 4 <br/>
          <input type="radio" value="3" name="clarity8" /> 3 <br/>
          <input type="radio" value="2" name="clarity8" /> 2 <br/>
          <input type="radio" value="1" name="clarity8" /> 1 <br/>
        </p>

        <p>9   writes key terms on blackboard or overhead screen</p>
        <p>
          <input type="radio" value="5" name="clarity9" /> 5 <br/>
          <input type="radio" value="4" name="clarity9" /> 4 <br/>
          <input type="radio" value="3" name="clarity9" /> 3 <br/>
          <input type="radio" value="2" name="clarity9" /> 2 <br/>
          <input type="radio" value="1" name="clarity9" /> 1 <br/>
        </p>
        <p><b>Enthusiasm:    expressive teaching behaviours that communicate instructor enthusiasm and  solicit student attention and interest</b></p>
        
        <p>10. speaks in a “dramatic” or expressive way</p>
        <p>
          <input type="radio" value="5" name="clarity10" /> 5 <br/>
          <input type="radio" value="4" name="clarity10" /> 4 <br/>
          <input type="radio" value="3" name="clarity10" /> 3 <br/>
          <input type="radio" value="2" name="clarity10" /> 2 <br/>
          <input type="radio" value="1" name="clarity10" /> 1 <br/>
        </p>

        <p>11. moves about the room while teaching</p>
        <p>
          <input type="radio" value="5" name="clarity11" /> 5 <br/>
          <input type="radio" value="4" name="clarity11" /> 4 <br/>
          <input type="radio" value="3" name="clarity11" /> 3 <br/>
          <input type="radio" value="2" name="clarity11" /> 2 <br/>
          <input type="radio" value="1" name="clarity11" /> 1 <br/>
        </p>

        <p>12. gestures with head, body, hands, or arms</p>
        <p>
          <input type="radio" value="5" name="clarity12" /> 5 <br/>
          <input type="radio" value="4" name="clarity12" /> 4 <br/>
          <input type="radio" value="3" name="clarity12" /> 3 <br/>
          <input type="radio" value="2" name="clarity12" /> 2 <br/>
          <input type="radio" value="1" name="clarity12" /> 1 <br/>
        </p>

        <p>13. exhibits facial gestures or expressions</p>
        <p>
          <input type="radio" value="5" name="clarity13" /> 5 <br/>
          <input type="radio" value="4" name="clarity13" /> 4 <br/>
          <input type="radio" value="3" name="clarity13" /> 3 <br/>
          <input type="radio" value="2" name="clarity13" /> 2 <br/>
          <input type="radio" value="1" name="clarity13" /> 1 <br/>
        </p>

        <p>14. avoids eye contact with students</p>
        <p>
          <input type="radio" value="5" name="clarity14" /> 5 <br/>
          <input type="radio" value="4" name="clarity14" /> 4 <br/>
          <input type="radio" value="3" name="clarity14" /> 3 <br/>
          <input type="radio" value="2" name="clarity14" /> 2 <br/>
          <input type="radio" value="1" name="clarity14" /> 1 <br/>
        </p>

        <p>15. tells jokes or humorous anecdotes</p>
        <p>
          <input type="radio" value="5" name="clarity15" /> 5 <br/>
          <input type="radio" value="4" name="clarity15" /> 4 <br/>
          <input type="radio" value="3" name="clarity15" /> 3 <br/>
          <input type="radio" value="2" name="clarity15" /> 2 <br/>
          <input type="radio" value="1" name="clarity15" /> 1 <br/>
        </p>

        <p>16. reads lecture verbatim from prepared notes or text</p>
        <p>
          <input type="radio" value="5" name="clarity16" /> 5 <br/>
          <input type="radio" value="4" name="clarity16" /> 4 <br/>
          <input type="radio" value="3" name="clarity16" /> 3 <br/>
          <input type="radio" value="2" name="clarity16" /> 2 <br/>
          <input type="radio" value="1" name="clarity16" /> 1 <br/>
        </p>

        <p>17. smiles or laughs while teaching</p>
        <p>
          <input type="radio" value="5" name="clarity17" /> 5 <br/>
          <input type="radio" value="4" name="clarity17" /> 4 <br/>
          <input type="radio" value="3" name="clarity17" /> 3 <br/>
          <input type="radio" value="2" name="clarity17" /> 2 <br/>
          <input type="radio" value="1" name="clarity17" /> 1 <br/>
        </p>

        <p><b>Interaction: teaching behaviours that foster student interaction and class participation</b></p>
        <p>18. encourages students to ask questions or make comments</p>
        <p>
          <input type="radio" value="5" name="clarity18" /> 5 <br/>
          <input type="radio" value="4" name="clarity18" /> 4 <br/>
          <input type="radio" value="3" name="clarity18" /> 3 <br/>
          <input type="radio" value="2" name="clarity18" /> 2 <br/>
          <input type="radio" value="1" name="clarity18" /> 1 <br/>
        </p>
        <p>19. praises students for good ideas</p>
        <p>
          <input type="radio" value="5" name="clarity19" /> 5 <br/>
          <input type="radio" value="4" name="clarity19" /> 4 <br/>
          <input type="radio" value="3" name="clarity19" /> 3 <br/>
          <input type="radio" value="2" name="clarity19" /> 2 <br/>
          <input type="radio" value="1" name="clarity19" /> 1 <br/>
        </p>

        <p>20. asks questions of individual students</p>
        <p>
          <input type="radio" value="5" name="clarity20" /> 5 <br/>
          <input type="radio" value="4" name="clarity20" /> 4 <br/>
          <input type="radio" value="3" name="clarity20" /> 3 <br/>
          <input type="radio" value="2" name="clarity20" /> 2 <br/>
          <input type="radio" value="1" name="clarity20" /> 1 <br/>
        </p>
        <p>21. asks questions of class as a whole</p>
        <p>
          <input type="radio" value="5" name="clarity21" /> 5 <br/>
          <input type="radio" value="4" name="clarity21" /> 4 <br/>
          <input type="radio" value="3" name="clarity21" /> 3 <br/>
          <input type="radio" value="2" name="clarity21" /> 2 <br/>
          <input type="radio" value="1" name="clarity21" /> 1 <br/>
        </p>

        <p>22. incorporates student ideas into lecture</p>
        <p>
          <input type="radio" value="5" name="clarity22" /> 5 <br/>
          <input type="radio" value="4" name="clarity22" /> 4 <br/>
          <input type="radio" value="3" name="clarity22" /> 3 <br/>
          <input type="radio" value="2" name="clarity22" /> 2 <br/>
          <input type="radio" value="1" name="clarity22" /> 1 <br/>
        </p>

        <p>23. uses a variety of media and activities in class</p>
        <p>
          <input type="radio" value="5" name="clarity23" /> 5 <br/>
          <input type="radio" value="4" name="clarity23" /> 4 <br/>
          <input type="radio" value="3" name="clarity23" /> 3 <br/>
          <input type="radio" value="2" name="clarity23" /> 2 <br/>
          <input type="radio" value="1" name="clarity23" /> 1 <br/>
        </p>

        <p>24. criticizes student when they make errors</p>
        <p>
          <input type="radio" value="5" name="clarity24" /> 5 <br/>
          <input type="radio" value="4" name="clarity24" /> 4 <br/>
          <input type="radio" value="3" name="clarity24" /> 3 <br/>
          <input type="radio" value="2" name="clarity24" /> 2 <br/>
          <input type="radio" value="1" name="clarity24" /> 1 <br/>
        </p>

        <p><b>Organization:  teaching behaviours that serve to structure or organize the subject matter</b></p>
        <p>25. reviews topics from previous lectures</p>
        <p>
          <input type="radio" value="5" name="clarity25" /> 5 <br/>
          <input type="radio" value="4" name="clarity25" /> 4 <br/>
          <input type="radio" value="3" name="clarity25" /> 3 <br/>
          <input type="radio" value="2" name="clarity25" /> 2 <br/>
          <input type="radio" value="1" name="clarity25" /> 1 <br/>
        </p>
         <p>26. gives preliminary overview of lecture</p>
        <p>
          <input type="radio" value="5" name="clarity26" /> 5 <br/>
          <input type="radio" value="4" name="clarity26" /> 4 <br/>
          <input type="radio" value="3" name="clarity26" /> 3 <br/>
          <input type="radio" value="2" name="clarity26" /> 2 <br/>
          <input type="radio" value="1" name="clarity26" /> 1 <br/>
        </p>
        <p>27. puts outline of lecture on blackboard or overhead screen</p>
        <p>
          <input type="radio" value="5" name="clarity27" /> 5 <br/>
          <input type="radio" value="4" name="clarity27" /> 4 <br/>
          <input type="radio" value="3" name="clarity27" /> 3 <br/>
          <input type="radio" value="2" name="clarity27" /> 2 <br/>
          <input type="radio" value="1" name="clarity27" /> 1 <br/>
        </p>

      
        <p>28. signals transition from one topic to the next</p>
        <p>
          <input type="radio" value="5" name="clarity28" /> 5 <br/>
          <input type="radio" value="4" name="clarity28" /> 4 <br/>
          <input type="radio" value="3" name="clarity28" /> 3 <br/>
          <input type="radio" value="2" name="clarity28" /> 2 <br/>
          <input type="radio" value="1" name="clarity28" /> 1 <br/>
        </p>
        <p>29. explains how each topic fits into the course as a whole</p>
        <p>
          <input type="radio" value="5" name="clarity29" /> 5 <br/>
          <input type="radio" value="4" name="clarity29" /> 4 <br/>
          <input type="radio" value="3" name="clarity29" /> 3 <br/>
          <input type="radio" value="2" name="clarity29" /> 2 <br/>
          <input type="radio" value="1" name="clarity29" /> 1 <br/>
        </p>
        <p>30. periodically summarizes points previously made</p>
        <p>
          <input type="radio" value="5" name="clarity30" /> 5 <br/>
          <input type="radio" value="4" name="clarity30" /> 4 <br/>
          <input type="radio" value="3" name="clarity30" /> 3 <br/>
          <input type="radio" value="2" name="clarity30" /> 2 <br/>
          <input type="radio" value="1" name="clarity30" /> 1 <br/>
        </p>

        <p><b>Pacing: teaching behaviours that affect rate of presentation of course content</b></p>
        <p>31. dwells excessively on obvious points</p>
        <p>
          <input type="radio" value="5" name="clarity31" /> 5 <br/>
          <input type="radio" value="4" name="clarity31" /> 4 <br/>
          <input type="radio" value="3" name="clarity31" /> 3 <br/>
          <input type="radio" value="2" name="clarity31" /> 2 <br/>
          <input type="radio" value="1" name="clarity31" /> 1 <br/>
        </p>
        <p>32. digresses from major theme of lecture</p>
        <p>
          <input type="radio" value="5" name="clarity32" /> 5 <br/>
          <input type="radio" value="4" name="clarity32" /> 4 <br/>
          <input type="radio" value="3" name="clarity32" /> 3 <br/>
          <input type="radio" value="2" name="clarity32" /> 2 <br/>
          <input type="radio" value="1" name="clarity32" /> 1 <br/>
        </p>
        <p>33. asks if students understand before proceeding to next topic</p>
        <p>
          <input type="radio" value="5" name="clarity33" /> 5 <br/>
          <input type="radio" value="4" name="clarity33" /> 4 <br/>
          <input type="radio" value="3" name="clarity33" /> 3 <br/>
          <input type="radio" value="2" name="clarity33" /> 2 <br/>
          <input type="radio" value="1" name="clarity33" /> 1 <br/>
        </p>
        <p>34. sticks to the point in answering students’ questions </p>
        <p>
          <input type="radio" value="5" name="clarity34" /> 5 <br/>
          <input type="radio" value="4" name="clarity34" /> 4 <br/>
          <input type="radio" value="3" name="clarity34" /> 3 <br/>
          <input type="radio" value="2" name="clarity34" /> 2 <br/>
          <input type="radio" value="1" name="clarity34" /> 1 <br/>
        </p>
        <p><b>Disclosure: teaching behaviours that serve to clarify course requirements and grading criteria</b></p>
        <p>35. advises students how to prepare for tests or exams</p>
        <p>
          <input type="radio" value="5" name="clarity35" /> 5 <br/>
          <input type="radio" value="4" name="clarity35" /> 4 <br/>
          <input type="radio" value="3" name="clarity35" /> 3 <br/>
          <input type="radio" value="2" name="clarity35" /> 2 <br/>
          <input type="radio" value="1" name="clarity35" /> 1 <br/>
        </p>
        <p>36. provides sample exam questions</p>
        <p>
          <input type="radio" value="5" name="clarity36" /> 5 <br/>
          <input type="radio" value="4" name="clarity36" /> 4 <br/>
          <input type="radio" value="3" name="clarity36" /> 3 <br/>
          <input type="radio" value="2" name="clarity36" /> 2 <br/>
          <input type="radio" value="1" name="clarity36" /> 1 <br/>
        </p>
        <p>37. tells students exactly what is expected on tests, essays, or assignments</p>
        <p>
          <input type="radio" value="5" name="clarity37" /> 5 <br/>
          <input type="radio" value="4" name="clarity37" /> 4 <br/>
          <input type="radio" value="3" name="clarity37" /> 3 <br/>
          <input type="radio" value="2" name="clarity37" /> 2 <br/>
          <input type="radio" value="1" name="clarity37" /> 1 <br/>
        </p>
        <p>38. states objectives of course as a whole</p>
        <p>
          <input type="radio" value="5" name="clarity38" /> 5 <br/>
          <input type="radio" value="4" name="clarity38" /> 4 <br/>
          <input type="radio" value="3" name="clarity38" /> 3 <br/>
          <input type="radio" value="2" name="clarity38" /> 2 <br/>
          <input type="radio" value="1" name="clarity38" /> 1 <br/>
        </p>
        <p>39. states objectives of each class session</p>
        <p>
          <input type="radio" value="5" name="clarity39" /> 5 <br/>
          <input type="radio" value="4" name="clarity39" /> 4 <br/>
          <input type="radio" value="3" name="clarity39" /> 3 <br/>
          <input type="radio" value="2" name="clarity39" /> 2 <br/>
          <input type="radio" value="1" name="clarity39" /> 1 <br/>
        </p>
        <p>40. reminds students of test dates and assignment deadlines</p>
        <p>
          <input type="radio" value="5" name="clarity40" /> 5 <br/>
          <input type="radio" value="4" name="clarity40" /> 4 <br/>
          <input type="radio" value="3" name="clarity40" /> 3 <br/>
          <input type="radio" value="2" name="clarity40" /> 2 <br/>
          <input type="radio" value="1" name="clarity40" /> 1 <br/>
        </p>
        <p><b>Speech: teaching behaviours relating to voice characteristics</b></p>
        <p>41. stutters, mumbles, or slurs words</p>
        <p>
          <input type="radio" value="5" name="clarity41" /> 5 <br/>
          <input type="radio" value="4" name="clarity41" /> 4 <br/>
          <input type="radio" value="3" name="clarity41" /> 3 <br/>
          <input type="radio" value="2" name="clarity41" /> 2 <br/>
          <input type="radio" value="1" name="clarity41" /> 1 <br/>
        </p>
        <p>42. speaks at appropriate volume</p>
        <p>
          <input type="radio" value="5" name="clarity42" /> 5 <br/>
          <input type="radio" value="4" name="clarity42" /> 4 <br/>
          <input type="radio" value="3" name="clarity42" /> 3 <br/>
          <input type="radio" value="2" name="clarity42" /> 2 <br/>
          <input type="radio" value="1" name="clarity42" /> 1 <br/>
        </p>
        <p>43. speaks at appropriate pace</p>
        <p>
          <input type="radio" value="5" name="clarity43" /> 5 <br/>
          <input type="radio" value="4" name="clarity43" /> 4 <br/>
          <input type="radio" value="3" name="clarity43" /> 3 <br/>
          <input type="radio" value="2" name="clarity43" /> 2 <br/>
          <input type="radio" value="1" name="clarity43" /> 1 <br/>
        </p>
        <p>44. speaks in monotone</p>
        <p>
          <input type="radio" value="5" name="clarity44" /> 5 <br/>
          <input type="radio" value="4" name="clarity44" /> 4 <br/>
          <input type="radio" value="3" name="clarity44" /> 3 <br/>
          <input type="radio" value="2" name="clarity44" /> 2 <br/>
          <input type="radio" value="1" name="clarity44" /> 1 <br/>
        </p>
        <p>45. speaks clearly</p>
        <p>
          <input type="radio" value="5" name="clarity45" /> 5 <br/>
          <input type="radio" value="4" name="clarity45" /> 4 <br/>
          <input type="radio" value="3" name="clarity45" /> 3 <br/>
          <input type="radio" value="2" name="clarity45" /> 2 <br/>
          <input type="radio" value="1" name="clarity45" /> 1 <br/>
        </p>
        <p><b>Rapport: teaching behaviours that contribute to quality of interpersonal relations between
               teacher and students</b></p>
        <p>46. addresses individual students by name</p>
        <p>
          <input type="radio" value="5" name="clarity46" /> 5 <br/>
          <input type="radio" value="4" name="clarity46" /> 4 <br/>
          <input type="radio" value="3" name="clarity46" /> 3 <br/>
          <input type="radio" value="2" name="clarity46" /> 2 <br/>
          <input type="radio" value="1" name="clarity46" /> 1 <br/>
        </p>
        <p>47. announces availability for consultation outside of class</p>
        <p>
          <input type="radio" value="5" name="clarity47" /> 5 <br/>
          <input type="radio" value="4" name="clarity47" /> 4 <br/>
          <input type="radio" value="3" name="clarity47" /> 3 <br/>
          <input type="radio" value="2" name="clarity47" /> 2 <br/>
          <input type="radio" value="1" name="clarity47" /> 1 <br/>
        </p>
        <p>48. offers to help students with problems</p>
        <p>
          <input type="radio" value="5" name="clarity48" /> 5 <br/>
          <input type="radio" value="4" name="clarity48" /> 4 <br/>
          <input type="radio" value="3" name="clarity48" /> 3 <br/>
          <input type="radio" value="2" name="clarity48" /> 2 <br/>
          <input type="radio" value="1" name="clarity48" /> 1 <br/>
        </p>
        <p>49. talks with students before or after class</p>
        <p>
          <input type="radio" value="5" name="clarity49" /> 5 <br/>
          <input type="radio" value="4" name="clarity49" /> 4 <br/>
          <input type="radio" value="3" name="clarity49" /> 3 <br/>
          <input type="radio" value="2" name="clarity49" /> 2 <br/>
          <input type="radio" value="1" name="clarity49" /> 1 <br/>
        </p>
        <p>50. shows tolerance of student differences</p>
        <p>
          <input type="radio" value="5" name="clarity50" /> 5 <br/>
          <input type="radio" value="4" name="clarity50" /> 4 <br/>
          <input type="radio" value="3" name="clarity50" /> 3 <br/>
          <input type="radio" value="2" name="clarity50" /> 2 <br/>
          <input type="radio" value="1" name="clarity50" /> 1 <br/>
        </p>
        <?php
         echo '<button class="btn btn-primary btn-md" onclick="getradio('."'".$teacherid."'".')">Submit</button>'

        ?>
        
      </div>
    </div>
  </div>
</div>

