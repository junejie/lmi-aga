<?php

$type = $_GET['tpe'];
$label = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V',
				'W','X','Y','Z');

if($type=='MULTIPLEC'){



	echo '<div class="form-group" id="qno-mc-group">
	        <label>Question No: <b class="text-danger">(Required)</b></label>
	        <input type="text" class="form-control" onkeypress="return isNumberKey(event)" id="qno" style="width:10%;">
	      </div>';
	echo '<b class="text-danger">Note: Atleast one question field(Question,Question(img)) must be filled</b>';      
	echo '<div class="form-group" id="q-mc-group">
	        <label>Question:</label>
	        <textarea class="form-control" id="qtext"></textarea>
	      </div>';


	echo '<form id="imgqform" method="post" enctype="multipart/form-data">';
	echo '<div class="form-group" style="display:none;">
	      	<label>Question(img):</label>
	      	<input type="file" class="form-control" id="imgq" name="myfile">
	      	<p id="imgq-loading"></p>
	      </div>';

	echo '</form>';
	//echo '<button onclick="up()">Click me</button>';
	echo '<div class="form-group" id="level-group">
			<div class="col-lg-12">
	        	
	        	<hr/>
	        </div>
	        <div class="col-lg-3">
	        	<label>Label: </label>';
	        	for($x=0;$x<5;$x++){
	        		echo '<div class="form-group" id="lbl-mc'.$x.'">';
	        		echo '<select class="form-control" id="label-'.$x.'"><option></option>';
	        		for($y=0;$y<=count($label)-1;$y++){
	        			echo '<option value="'.$label[$y].'">'.$label[$y].'</option>';
	        		}
	        		echo '</select>';
	        		echo '</div>';
	        	}

	 echo '</div>
	        <div class="col-lg-3">
	        	<label>Choices: </label>
	        	<div class="form-group">
		        	<input type="text" class="form-control" id="choice-a" >
		        </div>
		        <div class="form-group">
		        	<input type="text" class="form-control" id="choice-b" >
		        </div>
		        <div class="form-group">
		        	<input type="text" class="form-control" id="choice-c" >
		        </div>
		        <div class="form-group">
		        	<input type="text" class="form-control" id="choice-d" >
		        </div>
		        <div class="form-group">
		        	<input type="text" class="form-control" id="choice-e" >
		        </div>
		    </div>

		    <div class="col-lg-6" style="display:none;">

		    <form id="imgchoice-a-form" action="upload.php" method="post" enctype="multipart/form-data">
		    	<label>Choices(img): </label>
		    	<div class="form-group">
		    		<input type="file" class="form-control btn-xs" name="myfile" id="imgchoice-a">
		    		<p id="imgchoice-a-loading"></p>
		    	</div>	
		    </form>	

		    <form id="imgchoice-b-form" action="upload.php" method="post" enctype="multipart/form-data">
		    	<div class="form-group">
		    		<input type="file" class="form-control btn-xs" name="myfile" id="imgchoice-b" >
		    		<p id="imgchoice-b-loading"></p>
		    	</div>
		    </form>

		    <form id="imgchoice-c-form" action="upload.php" method="post" enctype="multipart/form-data">
		    	<div class="form-group">
		    		<input type="file" class="form-control btn-xs" name="myfile" id="imgchoice-c" >
		    		<p id="imgchoice-c-loading"></p>
		    	</div>
		    </form>

		    <form id="imgchoice-d-form" action="upload.php" method="post" enctype="multipart/form-data">
		    	<div class="form-group">
		    		<input type="file" class="form-control btn-xs" name="myfile" id="imgchoice-d" >
		    		<p id="imgchoice-d-loading"></p>
		    	</div>
		    </form>

		    <form id="imgchoice-e-form" action="upload.php" method="post" enctype="multipart/form-data">
		    	<div class="form-group">
		    		<input type="file" class="form-control btn-xs" name="myfile" id="imgchoice-e" >
		    		<p id="imgchoice-e-loading"></p>
		    	</div>
		    </form>
		    </div>

	      </div>';


	 echo '<div class="form-group">
		 		<div class="col-lg-12">
				    <hr/>
				   	<div class="form-group" id="ans-mc-group">
			        	<label>Answer: <b class="text-danger">(Required)</b></label>

			        	<input type="text" class="form-control uppercase" id="answer">
			       		<div class="text-danger" align="center" id="val-mes"></div>
			       	</div>
				    <button class="btn btn-primary btn-md" id="btnmc" onClick="savemc()">Save</button>
			    </div>

		    </div>';


}else if($type=='FILLIN'){
	echo '<div class="form-group" id="qno-fillin-group">
	        <label>Question No: <b class="text-danger">(Required)</b></label>
	        <input type="text" class="form-control" onkeypress="return isNumberKey(event)" id="qno-fillin" style="width:10%;">
	      </div>';
	      
	echo '<div class="form-group" id="q-fillin-group">
	        <label>Question: <b class="text-danger">(Required)</b></label>
	        <textarea class="form-control" id="qtext-fillin"></textarea>
	      </div>';
	echo '<div class="form-group" id="answer-fillin-group">
			<label>Answer: <b class="text-danger">(Required)</b></label>

			<input type="text" class="form-control uppercase" id="answer-fillin">
			<div class="text-danger" align="center" id="val-mes"></div>
			<br/>
			<p class="text-danger" align="center" id="fillin-validation"></p>
			<button class="btn btn-primary btn-md" id="btnfillin" onClick="savefillin()">Save</button>
		  </div>';
}else if($type=="AGDIS"){
	echo '<div class="form-group" id="qno-ad-group">
	        <label>Question No: <b class="text-danger">(Required)</b></label>
	        <input type="text" class="form-control" onkeypress="return isNumberKey(event)" id="qno-ad" style="width:10%;">
	      </div>';
	      
	echo '<div class="form-group" id="q-ad-group">
	        <label>Question: <b class="text-danger">(Required)</b></label>
	        <textarea class="form-control" id="qtext-ad"></textarea>
	      </div>';


	
	echo	'<div class="col-md-2">
				
				<label>Value: </label>';
				for($x=1;$x<=1;$x++){
					echo '<div class="form-group" id="val'.$x.'-ad-group">';
					echo '<select class="form-control" id="cmb-ad'.$x.'"><option></option>';
					for($y=0;$y<=count($label)-1;$y++){
	        			echo '<option value="'.$label[$y].'">'.$label[$y].'</option>';
	        		}
					echo '</select>';
					echo '</div>';
				}
	echo	'</div>
			 <div class="col-md-10">
			 	<label>Choices: </label>
			 	<div class="form-group" id="choice1-ad-group">
				 	<input type="text" class="form-control" id="ad-choice1"/>
				</div>
				

				 	
			 	
			 </div>

			 <div class="col-md-12">
			 		<hr/>
				   	<div class="form-group" id="ans-mc-group">
			        	<label>Answer: <b class="text-danger">(Required)</b></label>

			        	<input type="text" class="form-control uppercase" id="answermt">
			       		
			       	</div>
			 	<p class="text-danger" align="center" id="ad-validation"></p>
			 	<button class="btn btn-primary btn-md" id="btnad" onClick="savead()">Save</button>
			 </div>
			
		 </div>';
		 



}

?>


<script src="js/navigation.js"></script>
<script type="text/javascript" src="tinymce/js/tinymce/tinymce.min.js"></script>
<script>

tinymce.init({
    selector: "#qtext",
    theme: "modern",
  
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 }); 

tinymce.init({
    selector: "#qtext-fillin",
    theme: "modern",
  
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 });

tinymce.init({
    selector: "#qtext-ad",
    theme: "modern",
  
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 });
</script>