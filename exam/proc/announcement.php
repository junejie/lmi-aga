<?php
session_start();
include_once('../sqlconnect.php');
 
if(isset($_GET['show'])){

}else{
?>
<div class="col-md-12">
    <div class="form-group" id="subset-title">
        <label> Title: <b class="text-danger">(Required)</b></label>
        <input id="id-title" type="text" class="form-control">
    </div>

    <div class="form-group" id="subset-date">
        <label> Date: <b class="text-danger">(Required)</b></label>
        <input id="id-date" type="date" class="form-control">
    </div>

    <div class="form-group" id="subset-announceto">
        <label> Announce To: <b class="text-danger">(Required)</b></label>
        <select class="form-control" id="announceto">
        <?php
            $qry = mysql_query("
                select 'All' as grade union SELECT grade FROM grade_level
            ");
            echo '<option value=""></option>';
            while ($result = mysql_fetch_array($qry)) {
                echo '<option value="'.$result['grade'].'">'.$result['grade'].'</option>';
            }
        ?>
        </select>
    </div>
    <div class="form-group" id="annoucement-group">
    	<label>Instruction: <b class="text-danger">(Required)</b></label>
    	<textarea id="txtannouncement" class="form-control" style="height:250px;">
        </textarea>
    </div>
    <div class="form-group">
    	<div id="val" align="center" class="text-danger"></div>
    	<button id="btnsaveins" class="btn btn-primary btn-lg" onclick="saveannouncement()">Save</button>
        <button id="btncancelins" class="btn btn-default btn-lg" onclick="cancelinstruction()">Cancel</button>
    </div>
  </div>
<?php   
    }
?>

<script type="text/javascript" src="tinymce/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
    selector: "#txtannouncement",
    theme: "modern",
  
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 }); 


</script>