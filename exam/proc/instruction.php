<?php
session_start();
include_once('../sqlconnect.php');
 
if(isset($_GET['show'])){

}else{
?>
<div class="col-md-12">
    <div class="form-group" id="subset-group">
        <label>Subset: <b class="text-danger">(Required)</b></label>
        <?php 

            $qry = mysql_query("SELECT * FROM subject");
            echo '<select class="form-control" id="sid">';
            echo '<option value=""></option>';
            while ($result = mysql_fetch_array($qry)) {
                echo '<option value="'.$result['subjectid'].'">'.$result['subjectname'].'</option>';
            }
            echo '</select>';
        ?>
    </div>
    <div class="form-group" id="instruction-group">
    	<label>Instruction: <b class="text-danger">(Required)</b></label>
    	<textarea id="txtinstruction" class="form-control" style="height:250px;"></textarea>

    </div>
    <div class="form-group">
    	<div id="val" align="center" class="text-danger"></div>
    	
    	<button id="btnsaveins" class="btn btn-primary btn-lg" onclick="saveinstruction()">Save</button>
        <button id="btncancelins" class="btn btn-default btn-lg" onclick="cancelinstruction()">Cancel</button>
    </div>
  </div>
<?php   
    }
?>

<script type="text/javascript" src="tinymce/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
    selector: "#txtinstruction",
    theme: "modern",
  
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 }); 


</script>