$(document).ready(function(){
	$('#typeoftest').load('proc/load-typeoftest.php');
	loadexamsubset();
});


//accept number only
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function addexamsubset(){
	tot = $.trim($('#tot').val().toUpperCase());
	subset = $.trim($('#subsetname').val().toUpperCase());
	timelimit = $.trim($('#timelimit').val().toUpperCase());
	toa = $.trim($('#toa').val().toUpperCase());
	
	if(tot=='NONE' || subset=='' || timelimit=='' || toa==''){
		$('.validation').html('Please complete all fields!');
	}else{
		$.ajax({
				type: "POST",
				url: "proc/add-examsubset.php",
				data: 'tot='+tot +'&subset='+subset +'&timelimit='+timelimit +'&toa='+toa,
				success: function(result){
					if(result=='success'){
						alert('Added');
						clearall();
						loadexamsubset();
					}else{

					}

				}
			});
	}

}

function loadexamsubset(){
	$('.datalist').html('<img src="image/loading.gif" style="position:absolute;margin:auto;top:0;bottom:0;left:0;right:0;" />');
	$('.datalist').load('proc/load-examsubset.php');
}

function getid(val,id){
	
	$('.datalist').load('proc/update-examsubset.php?sel='+val +'&id='+id);
}

function editsubset(){
	subid = $.trim($('#subsetid').val().toUpperCase());
	tot = $.trim($('#tot2').val().toUpperCase());
	subset = $.trim($('#subsetname2').val().toUpperCase());
	timelimit = $.trim($('#timelimit2').val().toUpperCase());
	toa = $.trim($('#toa2').val().toUpperCase());

	if(tot=='NONE' || subset=='' || timelimit=='' || toa==''){
		$('.validation2').html('Please complete all fields');
	}else{
		$.ajax({
				type: "POST",
				url: "proc/process-update-examsubset.php",
				data: 'tot='+tot +'&subset='+subset +'&timelimit='+timelimit +'&toa='+toa +'&subid='+subid,
				success: function(result){
					if(result=='success'){
						alert('Updated');
						loadexamsubset();
					}else{

					}
				}
			});
	}

}

//delete subset
function deletesubset(val){
	$.ajax({
			type: "POST",
			url: "proc/delete-examsubset.php",
			data: 'id='+val,
			success: function(result){
				if(result=='success'){
					alert('Deleted');
					loadexamsubset();
				}else{

				}
			}
		});
}
//clear all
function clearall(){
	
	$('#subsetname').val('');
	$('#timelimit').val('');
	$('#toa').val('');
}

//show hidden form
function showhidden(){
	$('#hides').slideToggle();
}