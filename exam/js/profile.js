


//add profile
function profiling () {
	fullname = $.trim($('#txtname').val().toUpperCase());
	gender = $.trim($('#cmbgender').val().toUpperCase());	
	age = $.trim($('#txtage').val());
	dot = $.trim($('#txtdot').val());
	educatt = $.trim($('#txteducatt').val().toUpperCase());
	curremp = $.trim($('#txtcurremp').val().toUpperCase());
	currpos = $.trim($('#txtcurrpos').val().toUpperCase());
	level = $.trim($("#cmblevel").val().toUpperCase());
	lvl = $("#cmblevel :selected").text();
	
	datastring = 'fullname=' + fullname + '&gender=' + gender + '&age=' + age
				+ '&dot=' + dot + '&educatt=' + educatt + '&curremp=' + curremp
				+ '&currpos=' + currpos + '&level=' + level + '&lvl=' + lvl;

	
	if(fullname=='' || gender=='' || age=='' || educatt=='' || level==''){
		if(fullname==''){
			$('#name-group').addClass('has-error');
		}else{
			$('#name-group').removeClass('has-error');
		}

		if(gender==''){
			$('#gender-group').addClass('has-error');
		}else{
			$('#gender-group').removeClass('has-error');
		}

		if(age==''){
			$('#age-group').addClass('has-error');
		}else{
			$('#age-group').removeClass('has-error');
		}

		if(educatt==''){
			$('#educatt-group').addClass('has-error');
		}else{
			$('#educatt-group').removeClass('has-error');
		}

		if(level==''){
			$('#level-group').addClass('has-error');
		}else{
			$('#level-group').removeClass('has-error');
		}

		$('#val').html('<b>Please complete all the required fields!</b>')
	}else{
		removehaserror();
		$('#val').empty();

		$.ajax({
			type: "POST",
			url: 'proc/profiling.php',
			data: datastring,
			success: function(result){
				
				if(result=='success'){
					window.location="profile.php"
				}else{
				
				}
			}
							
		});

	}
	
}

//remove has-error
function removehaserror(){
	$('#name-group').removeClass('has-error');
	$('#gender-group').removeClass('has-error');
	$('#age-group').removeClass('has-error');
	$('#educatt-group').removeClass('has-error');
	$('#level-group').removeClass('has-error');
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}



//clear textbox
function clearall(){
	$('#txtname').val('')
	$('#cmbgender').val('')
	$('#txtage').val('');
	
	$('#txteducatt').val('')
	$('#txtcurremp').val('')
	$('#txtcurrpos').val('')
	$('#txtpos').val('')
}


