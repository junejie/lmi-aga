$(document).ready(function() {
//VALIDATION
    $('#user-edit-form').bootstrapValidator({
        feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
        },    
        fields: {
            firstname: {
                validators: {
                    notEmpty: {
                        message: 'firstname'
                    }
                }
            },
            midname: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            lastname: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            contact: {
                validators: {
                    digits: {
                        message: 'contact'
                    },
                    notEmpty: {
                        message: 'The email address is required'
                    }
                }
            },
            address: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            bday: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            bday: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            }

        },
        submitHandler: function(validator, form, submitButton) {

            //Get information
            var firstname = $('input[name=firstname]').val();
            var midname = $('input[name=midname]').val();
            var lastname = $('input[name=lastname]').val();
            var gender = $('select[name=gender]').val();
            var bday = $('input[name=bday]').val();
            var address = $('input[name=address]').val();
            var email = $('input[name=email]').val();
            var contact = $('input[name=contact]').val();
            var password = $('input[name=password]').val();

            $('input[name=userid]').val($('#edit-user-label').html());
            alert( $('input[name=userid]').val());

            //hide modal
            $('#user-edit-modal').modal('hide');

                //ajax to send datas
                $.ajax({
                  type: 'POST',
                  url: 'proc/process-add-student.php',
                  data: $('#user-edit-form').serialize(),
                  success: function(msg){
                    if(msg){
                        alert(msg);
                        $('#tableResult').load('proc/process-view-teachers.php');

                    }else{
                        alert(msg);
                    }   
                },
                error: function(){
                  alert('wrong');
                }
              });//end ajax
            validator.resetForm();
            $('#user-edit-form').each(function(){
                this.reset();
            });

            $('#data_' + selectedUser).css('background-color', '#d9edf7');
            $('#data_' + selectedUser).animate({
                                backgroundColor: '#ffffff',
            }, 5000 );

        }
    });

});