$(document).ready(function(){

			$('select[name=exam]').change(function(){
				
				if($(this).val() != ''){
					$('div.set-container').load('proc/get-exam-set.php?examname='+$(this).val());

				}else{
					$('div.set-container').html('Please select exam first.');

				}
				
			});

			var examset = "";
			//Save click
			$('div.panel-body').on('change', 'select#selectSet', function(){
				examset = $(this).val();
			});

			
			//Save click
			$('div.panel-footer').on('click', '#btnSave', function(){

			removeValidation();
				
				if($('select[name=exam]').val() == '' || $('select[name=set]').val() == '' || $('input[name=question-no]').val() == '' || $('input[name=question]').val() == '' || $('textarea[name=choices]').val() == '' || $('input[name=answer]').val() == ''){
				

					if($('select[name=exam]').val() == ''){
                
						validationClass('[name=exam]','has-error');
						$('select[name=exam]').focus();
            
					}else{
					
						validationClass('[name=exam]','has-success');
					
					}

					if($('select[name=set]').val() == ''){
                
						validationClass('[name=set]','has-error');
						$('select[name=set]').focus();
            
					}else{
					
						validationClass('[name=set]','has-success');
					
					}					

					if($('input[name=question-no]').val() == ''){

						validationClass('[name=question-no]','has-error');
						$('input[name=question-no]').focus();

					}else{
					
						validationClass('[name=question-no]','has-success', '');
					
					}

					if($('input[name=question]').val() == ''){

						validationClass('[name=question]','has-error');
						$('input[name=question]').focus();

					}else{
					
						validationClass('[name=question]','has-success', '');
					
					}

					if($('textarea[name=choices]').val() == ''){

						validationClass('[name=choices]','has-error');
						$('textarea[name=choices]').focus();

					}else{
					
						validationClass('[name=choices]','has-success', '');
					
					}

					if($('input[name=answer]').val() == ''){

						validationClass('[name=answer]','has-error');
						$('input[name=answer]').focus();

					}else{
					
						validationClass('[name=answer]','has-success', '');
					
					}					
			
				}else{

					$('select[name=exam]').val()
					$('select[name=set]').val()
					$('input[name=question-no]').val()
					$('input[name=question]').val()
					$('textarea[name=choices]').val()
					$('input[name=answer]').val()


					ex = $('select[name=exam]').val();
					//examset = $('select[name=set]').val();
					situation = $('input[name=situation]').val();
					questionno = $('input[name=question-no]').val();
					question = $('input[name=question]').val();
					//imgquestion = $('#filename').text();
					choices = $('textarea[name=choices]').val();
					answer = $('input[name=answer]').val();
					
					$.ajax({
							type: "POST",
							url: "proc/process-add-exam.php",
							data: "exam="+ex+"&examset="+examset+"&situation="+situation+"&questionno="+questionno+"&question="+question+"&choices="+choices+"&answer="+answer,
							success: function(html){    
								if(html=='true'){
									modalSuc();
									modalRes();
								}else{
									alert('something went wrong!');
									alert(html);
								}
							},
							beforeSend:function(){
								modalLoad();
							}
					});
					
				
				}
				
			});


			/*
			EFFECT FUNCTIONS
			*/
			
			//Modal Loading
			function modalLoad(){
				$('#modal-alert').modal('show');//show loading modal
			}
			
			//Modal Suc
			function modalSuc(){
				setTimeout(function() { 
					$('img#loading').hide();
					$('h1#loading-text').text('Done!');
			
					setTimeout(function() { 
						$('h1#loading-text').text('Done!');
						$('#modal-alert').modal('hide'); 
						$('tbody#cassette-items').load('proc/get-cassette-info.php');
					
					}, 700);//hide loading modal
				
				}, 1000);//hide loading modal
			}

			function modalRes(){
				$('img#loading').show();
				$('h1#loading-text').text('Saving...');
				$('p.help-block').remove();

			}
			
			//Function for validation
            function validationClass(name,validationClass){
			
			//function validationClass(name,validationClass,validationError){
                $(name).parents('div[class^="form-group"]').addClass(validationClass);
			}

            function removeValidation(){
                //$('p.help-block').remove();
                $('div.form-group').removeClass('has-error has-success');
            }


});