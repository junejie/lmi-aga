$(document).ready(function(){
	loadlevellist();
	
	
});

function addlevel(){
	level = $.trim($('#level').val().toUpperCase());
	if(level==''){
		$('#level-group').addClass('has-error');
		$('.validation').text('Please fill up this field!');
	}else{
		$('#level-group').removeClass('has-error');
		$('.validation').empty()
		$.ajax({
			type: "POST",
			url: "proc/process-add-level.php",
			data: 'level='+level,
			success: function(result){
				if(result=='success'){
					alert('Added');
					loadlevellist();
					$('#level').val('');
				}else{

				}
			}
		});
	}
}

function loadlevellist(){
	$('.datalist').html('<img src="image/loading.gif" style="position:absolute;margin:auto;top:0;bottom:0;left:0;right:0;" />');
	$('.datalist').load('proc/levellist.php');
}

function editlevel(val){
	txt = $('#'+val).text();
	inputtxt = $('#txt'+val).text();
	
	if(txt=="Edit"){
		
		$('#'+val).text('Save');
		$('#txt'+val).html('<input type="text" id="'+val+'" style="width:50%;" class="uppercase" value="'+inputtxt+'" />')
	}else{
		lvl = $.trim($('input#'+val).val().toUpperCase());
		if(lvl==''){
			alert('Please fill up this field!');
			$('input#'+val).focus();
		}else{
			
			$.ajax({
				type: "POST",
				url: "proc/update-add-level.php",
				data: 'lvl='+lvl +'&id='+val,
				success: function(result){
					if(result=='success'){
						loadlevellist();
						
					}else{
						
					}
					$('#'+val).text('Edit');
					
				}
			});
		}


		
		
	}
}

function deletelevel(val){

		$.ajax({
			type: "POST",
			url: "proc/delete-add-level.php",
			data: 'id='+val,
			success: function(result){
				if(result=='success'){
					loadlevellist();
				}else{

				}
				
			}
		});
}

function showhidden(){
	
	$('#hides').slideToggle();
}