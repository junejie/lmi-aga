$(document).ready(function(){
	loadtypeoftestlist()

})

function loadtypeoftestlist(){
	$('.datalist').html('<img src="image/loading.gif" style="position:absolute;margin:auto;top:0;bottom:0;left:0;right:0;" />');
	$('.datalist').load('proc/typeoftest.php');
}

function addtypeoftest(){
	typeoftest = $.trim($('#typeoftest').val().toUpperCase());
	if(typeoftest==''){
		$('#typeoftest-group').addClass('has-error');
		$('.validation').text('Please fill up this field!');
	}else{
		$('#typeoftest-group').removeClass('has-error');
		$('.validation').empty()
		$.ajax({
			type: "POST",
			url: "proc/ins-typeoftest.php",
			data: 'typeoftest='+typeoftest,
			success: function(result){
				if(result=='success'){
					alert('Added');
					$('#typeoftest').val('');
					loadtypeoftestlist();
				}else{

				}
			}
		});
	}
}

//edit type of test
function edittot(val){
	txt = $('#'+val).text();
	inputtxt = $('#txt'+val).text();
	if(txt=="Edit"){
		$('#'+val).text('Save');
		$('#txt'+val).html('<input type="text" id="'+val+'" style="width:50%;" class="uppercase" value="'+inputtxt+'" />')

	}else{
		typeoftest = $.trim($('input#'+val).val().toUpperCase());
		if(typeoftest==''){
			alert('Please fill up this field!');
			$('input#'+val).focus();
		}else{

			$.ajax({
				type: "POST",
				url: "proc/update-typeoftest.php",
				data: 'typeoftest='+typeoftest +'&id='+val,
				success: function(result){
					if(result=='success'){
						loadtypeoftestlist();

					}else{
						
					}
					$('#'+val).text('Edit');
				}
			});

			
		}
	}
}

//delete type of test
function deletetot(val){

		$.ajax({
			type: "POST",
			url: "proc/delete-typeoftest.php",
			data: 'id='+val,
			success: function(result){
				if(result=='success'){
					loadtypeoftestlist();
				}else{

				}
				
			}
		});
}

//show hidden form
function showhidden(){
	$('#hides').slideToggle();
}