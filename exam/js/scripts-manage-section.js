$(document).ready(function() {

    //GLOBAL DEC
    var tdata;// USED TO UPDATE TABLE
    var selectedUser;

    console.log('load ready...')

    //Sidenav off canvas
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });




    //ADD EDIT DELETE TEACHERS

    //VIEW TEACHERS
    $(document.body).on('click', 'button#view-member', function(){

        var dataString = 'proc/process-view-section-info.php?id=' + $(this).val();

        $.getJSON(dataString, function(json) {
            $.each(json, function(x, datas) {   

                //Assign datas
                //alert(datas.sectionname);
                
                $('#userModalLabel').html('Subject: ' + datas.userid);
                $('#idsection').html(datas.sectionname);
                $('#idcurriculum').html(datas.curriculum);
                $('input[name="userid"]').val(datas.userid);
                //assign id to
                selectedUser = datas.userid;
                   
            });
        });
        

        $('#userModal').modal('show');
    });

    //EDIT MEMBERS

    $(document.body).on('click', '#editUser', function(){

        var label = $('#userModalLabel').html().split(" ");
        var section =  $('#idsection').html();
        var curriculum =  $('#idcurriculum').html();

        //aisgn id
        $('input[name=userid]').val(label[1]);
        $('input[name=userid]').hide();

        tdata = '#data_' + $('input[name=userid]').val();//set data what to update

        //hide user view
        $('#userModal').modal('hide');


        //add input fields
        
        $('input[name=sectionname]').val(section);
        $('input[name=curriculum]').val(curriculum);


        //show user
        setTimeout(function(){
            $('#user-edit-modal').modal('show'); 
        }, 1000);


    });

    //DELETE

    //WARNING
    $(document.body).on('click', '#delUser', function(){
        console.log('show confirm')
        $('#userModal').modal('hide');
        
        setTimeout(function(){
            $('#confirm-delete').modal('show');
        }, 1000);
    });

    
    $(document.body).on('click', '#addData', function(){
        $('#user-add-modal').modal('show');
    });

    //DELETE USER
    $(document.body).on('click', '#confirmDelete', function(){
        console.log('start delete...')

        $('#userModal').modal('hide');
        $('#confirm-delete').modal('hide');
        var id = $('input[name="userid"]').val();
        //ajax to del datas
        $.ajax({
          type: 'POST',
          url: 'proc/process-section-delete.php',
          data: 'id=' + id,
          success: function(msg){
            if(msg){

                alert(msg);
                //change info
                $('#tableResult').load('proc/process-view-section.php');
            }else{
                alert(msg);
            }   
        },
        error: function(){
          alert('wrong');
        }
      });//end ajax

        //hide deleted row
        $('#data_' + selectedUser).animate({ backgroundColor: "#fbc7c7" }, "fast").animate({ opacity: "hide" }, "slow");


    });


//VALIDATION

//add
$('#user-add-form').bootstrapValidator({
        feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
        },    
        fields: {
            sectionname: {
                validators: {
                    notEmpty: {
                        message: 'The Section is required'
                    }
                }
            },
            curiculum: {
                validators: {
                    notEmpty: {
                        message: 'The Curiculum is required'
                    }
                }
            }
        },
        submitHandler: function(validator, form, submitButton) {

            //Get information
            var sectionname = $('input[name=sectionname]').val();
            var curiculum = $('input[name=curiculum]').val();

            //hide modal
            $('#user-edit-modal').modal('hide');

                //ajax to send datas
                $.ajax({
                  type: 'POST',
                  url: 'proc/process-add-section.php',
                  data: $('#user-add-form').serialize(),
                  success: function(msg){
                    if(msg){

                        $('#user-add-modal').modal('hide');

                        //add message to alert
                        alert(msg);

                        //change info
                        $('#tableResult').load('proc/process-view-section.php');

                    }else{
                        alert(msg);
                    }   
                },
                error: function(){
                  alert('wrong');
                }
              });//end ajax


            //Reset form
            validator.resetForm();
            $('#user-edit-form').each(function(){
                this.reset();
            });

            $('#data_' + selectedUser).css('background-color', '#d9edf7');
            $('#data_' + selectedUser).animate({
                                backgroundColor: '#ffffff',
            }, 5000 );

        }
    });

    $('#user-edit-form').bootstrapValidator({
        feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
        },    
        fields: {
            address: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    }
                }
            }
        },
        submitHandler: function(validator, form, submitButton) {

            //hide modal
            $('#user-edit-modal').modal('hide');

                //ajax to send datas
                $.ajax({
                  type: 'POST',
                  url: 'proc/process-update-sections-info.php',
                  data: $('#user-edit-form').serialize(),
                  success: function(msg){
                    if(msg){



                        //add message to alert
                        alert(msg);

                        //change info
                        $('#tableResult').load('proc/process-view-section.php');

                    }else{
                        alert(msg);
                    }   
                },
                error: function(){
                  alert('wrong');
                }
              });//end ajax


            //Reset form
            validator.resetForm();
            $('#user-edit-form').each(function(){
                this.reset();
            });

            $('#data_' + selectedUser).css('background-color', '#d9edf7');
            $('#data_' + selectedUser).animate({
                                backgroundColor: '#ffffff',
            }, 5000 );

        }
    });

});