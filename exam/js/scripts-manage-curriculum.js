$(document).ready(function() {

    //GLOBAL DEC
    var tdata;// USED TO UPDATE TABLE
    var selectedUser;

    var subjectList = "";

    //Sidenav off canvas
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });




    //ADD EDIT DELETE TEACHERS

    //VIEW TEACHERS
    $(document.body).on('click', 'button#view-member', function(){
        //url to get datas

        subjectList = "";

        if($(this).hasClass('btn-success')){
            $(this).removeClass('btn-success').addClass('btn-danger');
            $(this).html('Remove <i class="glyphicon glyphicon glyphicon-remove"></i>');
        }else{
            $(this).removeClass('btn-danger').addClass('btn-success');
            $(this).html('Add <i class="glyphicon glyphicon glyphicon-plus"></i>');
        }

        $('#user-add-form button').each(function() {
              if($(this).hasClass('btn-danger')){
                subjectList += $(this).val();


              }
        });

        $('#user-edit-form button').each(function() {
              if($(this).hasClass('btn-danger')){
                subjectList += $(this).val();

              }
              //alert($(this).val());
        });
         $('input[name="address"]').val(subjectList);
         //alert(subjectList);
    });


    $(document.body).on('click', 'button#view', function(){
        //url to get datas
        $('#user-edit-modal').modal('show');
        $('input[name=email]').val($(this).val());
        selectedUser = $(this).val();
    });

    //EDIT MEMBERS

    $(document.body).on('click', '#editUser', function(){

        var label = $('#userModalLabel').html().split(" ");
        var address = $('#modal-address').html();
        var email = $('#modal-email').html();

        //aisgn id
        $('input[name=userid]').val(label[1]);
        $('input[name=userid]').hide();

        tdata = '#data_' + $('input[name=userid]').val();//set data what to update

        //hide user view
        $('#userModal').modal('hide');


        //add input fields
        
        $('input[name=email]').val(address);
        $('input[name=address]').val(email);


        //show user
        setTimeout(function(){
            $('#user-edit-modal').modal('show'); 
        }, 1000);


    });

    //DELETE

    //WARNING
    $(document.body).on('click', '#delUser', function(){
        $('#confirm-delete').modal('show');
    });

    
    $(document.body).on('click', '#addData', function(){
        $('#user-add-modal').modal('show');
    });

    //DELETE USER
    $(document.body).on('click', '#confirmDelete', function(){

        $('#user-edit-modal').modal('hide');
        $('#confirm-delete').modal('hide');

        //ajax to del datas
        $.ajax({
          type: 'POST',
          url: 'proc/process-curric-delete.php',
          data: 'userid=' + selectedUser,
          success: function(msg){
            if(msg){

                alert(msg);
                //change info
                $('#tableResult').load('proc/process-view-curric-list.php');
            }else{
                alert(msg);
            }   
        },
        error: function(){
          alert('wrong');
        }
      });//end ajax

        //hide deleted row
        $('#data_' + selectedUser).animate({ backgroundColor: "#fbc7c7" }, "fast").animate({ opacity: "hide" }, "slow");


    });


//VALIDATION

//add
$('#user-add-form').bootstrapValidator({
        feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
        },    
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'Curriculum Name is required'
                    }
                }
            }
        },
        submitHandler: function(validator, form, submitButton) {

            $('input[name=address]').val(subjectList);

            
            //Get information
            var address = $('input[name=address]').val();
            var email = $('input[name=email]').val();

            //hide modal
            $('#user-edit-modal').modal('hide');

                //ajax to send datas
                $.ajax({
                  type: 'POST',
                  url: 'proc/process-add-subjects-curic.php',
                  data: $('#user-add-form').serialize(),
                  success: function(msg){
                    if(msg){

                        $('#user-add-modal').modal('hide');

                        //add message to alert
                        alert(msg);

                        //change info
                        $('#tableResult').load('proc/process-view-curric-list.php');

                    }else{
                        alert(msg);
                    }   
                },
                error: function(){
                  alert('wrong');
                }
              });//end ajax
            

        }
    });

    $('#user-edit-form').bootstrapValidator({
        feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
        },    
        fields: {
            address: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    }
                }
            }
        },
        submitHandler: function(validator, form, submitButton) {

            //$('input[name=address]').val(subjectList);
            //hide modal
            $('#user-edit-modal').modal('hide');

                //ajax to send datas
                $.ajax({
                  type: 'POST',
                  url: 'proc/process-update-subjects-curic.php',
                  data: $('#user-edit-form').serialize(),
                  success: function(msg){
                    if(msg){



                        //add message to alert
                        alert(msg);

                        //change info
                        $('#tableResult').load('proc/process-view-curric-list.php');

                    }else{
                        alert(msg);
                    }   
                },
                error: function(){
                  alert('wrong');
                }
              });//end ajax


            //Reset form
            validator.resetForm();
            $('#user-edit-form').each(function(){
                this.reset();
            });

            $('#data_' + selectedUser).css('background-color', '#d9edf7');
            $('#data_' + selectedUser).animate({
                                backgroundColor: '#ffffff',
            }, 5000 );

        }
    });

});