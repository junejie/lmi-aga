$(document).ready(function() {

    //GLOBAL DEC
    var tdata;// USED TO UPDATE TABLE
    var selectedUser;

    //Sidenav off canvas
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });




    //ADD EDIT DELETE TEACHERS

    //VIEW TEACHERS
    $(document.body).on('click', 'button#view-member', function(){

        //url to get datas
        alert($(this).val());
        
        var dataString = 'proc/process-view-teachers-info.php?userid=' + $(this).val();

        $.getJSON(dataString, function(json) {
            $.each(json, function(x, datas) {   

                //Assign datas
                //alert(datas.firstname);
                $('#teacherid').html(datas.userid)
                $('#userModalLabel').html('Teacher: ' + datas.userid);
                $('#modal-fullname').html(datas.firstname + ' ' + datas.midname + ' ' + datas.lastname);
                $('#modal-type').html('Teacher');
                $('#modal-gender').html(datas.gender);
                $('#modal-birthday').html(datas.birthday);
                $('#modal-address').html(datas.address);
                $('#modal-contact').html(datas.contact);
                $('#modal-email').html(datas.email)
                $('#modal-status').html(datas.uStatus);
                $('#delUser').val(datas.uStatus);
                $('#modal-pic').attr('src','uploads/'+datas.picture);

                //assign id to
                selectedUser = datas.userid;
                   
            });
        });
        

        $('#userModal').modal('show');
    });

    //EDIT MEMBERS

    $(document.body).on('click', '#editUser', function(){

        var label = $('#userModalLabel').html().split(" ");
        var fullname = $('#modal-fullname').html().split(" ");
        var type = $('#modal-type').html();
        var address = $('#modal-address').html();
        var contact = $('#modal-contact').html();
        var email = $('#modal-email').html();
        var bday = $('#modal-birthday').html();

        //aisgn id
        $('input[name=userid]').val(label[1]);
        $('input[name=userid]').hide();

        tdata = '#data_' + $('input[name=userid]').val();//set data what to update

        //hide user view
        $('#userModal').modal('hide');


        //add input fields
        $('#edit-user-label').html(label[0] + ' ' + label[1]);
        $('input[name=firstname]').val(fullname[0]);
        $('input[name=midname]').val(fullname[1]);
        $('input[name=lastname]').val(fullname[2]);
        $('input[name=address]').val(address);
        $('input[name=contact]').val(contact);
        $('input[name=email]').val(email);
        $('input[name=bday]').val(bday);


        //show user
        setTimeout(function(){
            $('#user-edit-modal').modal('show'); 
        }, 1000);


    });

    //DELETE

    //WARNING
    $(document.body).on('click', '#delUser', function(){

        if($('#delUser').val() == 'Active'){
            $('p#confirmMessage').html('Are you sure you want to deactivate this account?');
        }else{
            $('p#confirmMessage').html('Are you sure you want to activate this account?');
        }

        $('#confirm-delete').modal('show');
    });

    //DELETE USER
    $(document.body).on('click', '#confirmDelete', function(){

        var uStatus = '';
        if($('#delUser').val() == 'Active'){
            uStatus = 'Unactive';
        }else{
            uStatus = 'Active';
        }

        $('#userModal').modal('hide');
        $('#confirm-delete').modal('hide');

        //ajax to del datas
        $.ajax({
          type: 'POST',
          url: 'proc/process-member-delete.php',
          data: 'userid=' + selectedUser + '&status=' + uStatus,
          success: function(msg){
            if(msg){

                alert(msg);
                //change info
                $('#tableResult').load('proc/process-view-teachers.php');
            }else{
                alert(msg);
            }   
        },
        error: function(){
          alert('wrong');
        }
      });//end ajax

        //hide deleted row
        $('#data_' + selectedUser).animate({ backgroundColor: "#fbc7c7" }, "fast").animate({ opacity: "hide" }, "slow");


    });


//VALIDATION
    $('#user-edit-form').bootstrapValidator({
        feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
        },    
        fields: {
            firstname: {
                validators: {
                    notEmpty: {
                        message: 'firstname'
                    }
                }
            },
            lastname: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            contact: {
                validators: {
                    digits: {
                        message: 'contact'
                    },
                    notEmpty: {
                        message: 'The email address is required'
                    }
                }
            },
            address: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            bday: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            bday: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            }

        },
        submitHandler: function(validator, form, submitButton) {

            //Get information
            var firstname = $('input[name=firstname]').val();
            var midname = $('input[name=midname]').val();
            var lastname = $('input[name=lastname]').val();
            var gender = $('select[name=gender]').val();
            var bday = $('input[name=bday]').val();
            var address = $('input[name=address]').val();
            var email = $('input[name=email]').val();
            var contact = $('input[name=contact]').val();
            var password = $('input[name=password]').val();

            //hide modal
            $('#user-edit-modal').modal('hide');

                //ajax to send datas
                $.ajax({
                  type: 'POST',
                  url: 'proc/process-update-teachers-info.php',
                  data: $('#user-edit-form').serialize(),
                  success: function(msg){
                    if(msg){



                        //add message to alert
                        alert(msg);

                        //change info
                        $('#tableResult').load('proc/process-view-teachers.php');

                    }else{
                        alert(msg);
                    }   
                },
                error: function(){
                  alert('wrong');
                }
              });//end ajax


            //Reset form
            validator.resetForm();
            $('#user-edit-form').each(function(){
                this.reset();
            });

            $('#data_' + selectedUser).css('background-color', '#d9edf7');
            $('#data_' + selectedUser).animate({
                                backgroundColor: '#ffffff',
            }, 5000 );

        }
    });

});

//uploader
//uploader
function upload(formid){
    var formElement = document.getElementById(formid);
    var id = $('#teacherid').text();
    var pics = document.getElementById('uploadpic').files[0];
    datastr = 'id=' + id + '&pics=' + pics.name;
        $.ajax({
          url: 'upload.php',
          type: 'POST',
          data: new FormData( formElement ),
          processData: false,
          contentType: false,
          success: function(result){

           $.ajax({ 
                  type: 'POST',
                  url: 'proc/updatepics2.php',
                  data: datastr,
                  success: function(msg){
                    if(msg == '1'){
                        alert('Done');
                        window.location = 'manage-teachers.php';
                    }

                    
                  }
              })
            
          }

            

        });
}