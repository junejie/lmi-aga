$(document).ready(function() {

    //GLOBAL DEC
    var tdata;// USED TO UPDATE TABLE
    var selectedUser;

    //Sidenav off canvas
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });




    //ADD EDIT DELETE TEACHERS

    //VIEW TEACHERS
    $(document.body).on('click', 'button#view-member', function(){

        //url to get datas
       // alert($(this).val());
        
        var dataString = 'proc/process-view-subjects-info.php?userid=' + $(this).val();

        $.getJSON(dataString, function(json) {
            $.each(json, function(x, datas) {   

                //Assign datas
                //alert(datas.firstname);
                
                $('#userModalLabel').html('Subject: ' + datas.userid);
                $('#modal-address').html(datas.address);
                $('#modal-email').html(datas.email);

                //assign id to
                selectedUser = datas.userid;
                   
            });
        });
        

        $('#userModal').modal('show');
    });

    //EDIT MEMBERS

    $(document.body).on('click', '#editUser', function(){

        var label = $('#userModalLabel').html().split(" ");
        var address = $('#modal-address').html();
        var email = $('#modal-email').html();

        //aisgn id
        $('input[name=userid]').val(label[1]);
        $('input[name=userid]').hide();

        tdata = '#data_' + $('input[name=userid]').val();//set data what to update

        //hide user view
        $('#userModal').modal('hide');


        //add input fields
        
        $('input[name=email]').val(address);
        $('input[name=address]').val(email);


        //show user
        setTimeout(function(){
            $('#user-edit-modal').modal('show'); 
        }, 1000);


    });

    //DELETE

    //WARNING
    $(document.body).on('click', '#delUser', function(){
        $('#confirm-delete').modal('show');
    });

    
    $(document.body).on('click', '#addData', function(){
        $('#user-add-modal').modal('show');
    });

    //DELETE USER
    $(document.body).on('click', '#confirmDelete', function(){

        $('#userModal').modal('hide');
        $('#confirm-delete').modal('hide');

        //ajax to del datas
        $.ajax({
          type: 'POST',
          url: 'proc/process-subjects-delete.php',
          data: 'userid=' + selectedUser,
          success: function(msg){
            if(msg){

                alert(msg);
                //change info
                $('#tableResult').load('proc/process-view-subjects.php');
            }else{
                alert(msg);
            }   
        },
        error: function(){
          alert('wrong');
        }
      });//end ajax

        //hide deleted row
        $('#data_' + selectedUser).animate({ backgroundColor: "#fbc7c7" }, "fast").animate({ opacity: "hide" }, "slow");


    });


//VALIDATION

//add
$('#user-add-form').bootstrapValidator({
        feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
        },    
        fields: {
            address: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    }
                }
            }
        },
        submitHandler: function(validator, form, submitButton) {

            //Get information
            var address = $('input[name=address]').val();
            var email = $('input[name=email]').val();
            var teacher = $('select[name=grade]').val();
            //hide modal
            $('#user-edit-modal').modal('hide');

                //ajax to send datas
                $.ajax({
                  type: 'POST',
                  url: 'proc/process-add-subjects.php',
                  data: $('#user-add-form').serialize(),
                  success: function(msg){
                    if(msg){

                        $('#user-add-modal').modal('hide');

                        //add message to alert
                        alert(msg);

                        //change info
                        $('#tableResult').load('proc/process-view-subjects.php');

                    }else{
                        alert(msg);
                    }   
                },
                error: function(){
                  alert('wrong');
                }
              });//end ajax


            //Reset form
            validator.resetForm();
            $('#user-edit-form').each(function(){
                this.reset();
            });

            $('#data_' + selectedUser).css('background-color', '#d9edf7');
            $('#data_' + selectedUser).animate({
                                backgroundColor: '#ffffff',
            }, 5000 );

        }
    });

    $('#user-edit-form').bootstrapValidator({
        feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
        },    
        fields: {
            address: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    }
                }
            }
        },
        submitHandler: function(validator, form, submitButton) {

            //Get information
            var address = $('input[name=address]').val();
            var email = $('input[name=email]').val();
            var teacher = $('select[name=teacheredit]').val();
            //hide modal
            $('#user-edit-modal').modal('hide');

                //ajax to send datas
                $.ajax({
                  type: 'POST',
                  url: 'proc/process-update-subjects-info.php',
                  data: $('#user-edit-form').serialize(),
                  success: function(msg){
                    if(msg){



                        //add message to alert
                        alert(msg);

                        //change info
                        $('#tableResult').load('proc/process-view-subjects.php');

                    }else{
                        alert(msg);
                    }   
                },
                error: function(){
                  alert('wrong');
                }
              });//end ajax


            //Reset form
            validator.resetForm();
            $('#user-edit-form').each(function(){
                this.reset();
            });

            $('#data_' + selectedUser).css('background-color', '#d9edf7');
            $('#data_' + selectedUser).animate({
                                backgroundColor: '#ffffff',
            }, 5000 );

        }
    });

});