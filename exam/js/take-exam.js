var ctr=0;
$(document).ready(function(){
  function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
  $(document).on("keydown", disableF5);
  
  //$('#btn'+first).prop("disabled",false);
  //$('.ss').click(function(){
      //$(this).prop("disabled",true);
      //$('#'+btnarray[ctr+=1]).prop("disabled",false);
  //})
  
  //multiple choice
  $('.rbn-choices').click(function(){
    cont = $(this).attr('name');
    answer=$(this).val();
    qid = cont.substring(cont.indexOf('-')+1);
    $('#ans'+qid).text(answer);
    $('#rbnval'+qid).text(answer);
    $('#b'+qid).html('<span class="glyphicon glyphicon-ok"></span>')
  })

  //agree disagree question
  $('.adchoice').click(function(){
    cont = $(this).attr('name');
    answer=$(this).val();
    qid = cont.substring(cont.indexOf('-')+1);
    
    $('#ans'+qid).text(answer);
    $('#rbnval'+qid).text(answer);
    $('#b'+qid).html('<span class="glyphicon glyphicon-ok"></span>')
  })
  
})

function loadquestion(subsetid,timelimit){
	 //myWindow = window.open("proc/question.php?subsetid="+subsetid, "", "width=800, height=500");
   
     popupwindow("question.php?sid="+subsetid+"&timelimit="+timelimit, "", '900', '500');

}

function popupwindow(url, title, w, h) {
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, "", 'toolbar=no, location=no, directories=no, status=0, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

function countdown(){

		m=30;
		s=60;

	var interval = setInterval(function() {
		
    	s--;
    	$('#tmr').text(m+':'+s);

    	// Display 'counter' wherever you want to display it.
    	if (s <= 0) {
        	m--;
        	s=60;
        	 
    	}
	}, 1000);
}

//start exam
function startexam(subsetid,timelimit){
  
    window.location="loadquestion.php?sid="+subsetid+"&timelimit="+timelimit;
}


 
function getans(val){
  alert(val);
}

//submit multiple choice answers
function submitmcexam(subsetid,pid,stat){
  if(stat=="CLICK"){
    if(confirm('Once submitted it cannot be undone!')){
      //get the answer of the examinee
      var answer = $('.ans').map( function() {
          name = $(this).text(); 
          return name;
      }).get();
      //get the correct answer
      var corranswer = $('.corrans').map( function() {
          txt = $(this).text();
          return txt;
      }).get();
      //get question id
      var qids = $('.page').map( function() {
          txt = $(this).attr('id');
          return txt.substring(txt.indexOf('q')+1);
      }).get();

      for(x=0;x<corranswer.length;x++){
          qid = qids[x];
          if(corranswer[x].toUpperCase()==answer[x].toUpperCase()){
            response = "CORRECT";
          }else{
            response = "WRONG";
          }
        strdata = 'qid=' + qid + '&response=' + response + '&answer=' + answer[x]
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=MULTIPLE';

        $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){ 
                  window.opener.location="exam.php";
                  window.close();

                }   
                    
              });  
      }

      //redirect(subsetid,pid);

    }
  }else{
      //get the answer of the examinee
      var answer = $('.ans').map( function() {
          name = $(this).text(); 
          return name;
      }).get();
      //get the correct answer
      var corranswer = $('.corrans').map( function() {
          txt = $(this).text();
          return txt;
      }).get();
      //get question id
      var qids = $('.page').map( function() {
          txt = $(this).attr('id');
          return txt.substring(txt.indexOf('q')+1);
      }).get();

      for(x=0;x<corranswer.length;x++){
          qid = qids[x];
          if(corranswer[x].toUpperCase()==answer[x].toUpperCase()){
            response = "CORRECT";
          }else{
            response = "WRONG";
          }
        strdata = 'qid=' + qid + '&response=' + response + '&answer=' + answer[x]
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=MULTIPLE';
        $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){ 
                  
                }   
                    
              });
      }

      redirect(subsetid,pid);
       
  }

 
}

//submit fillin answer 
function submitfitbexam(subsetid,pid,stat){
  if(stat=="CLICK"){
    if(confirm('Once submitted it cannot be undone!')){
      //get the answer of the examinee
      var answer = $('.ans').map( function() {
          name = $(this).text(); 
          return name;
      }).get();
      //get the correct answer
      var corranswer = $('.corrans').map( function() {
          txt = $(this).text();
          return txt;
      }).get();
      //get question id
      var qids = $('.page').map( function() {
          txt = $(this).attr('id');
          return txt.substring(txt.indexOf('q')+1);
      }).get();

      for(x=0;x<corranswer.length;x++){
          qid = qids[x];
          if(corranswer[x].toUpperCase()==answer[x].toUpperCase()){
            response = "CORRECT";
          }else{
            response = "WRONG";
          }
        strdata = 'qid=' + qid + '&response=' + response + '&answer=' + answer[x]
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=FILLIN';

        $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){
                }   
                    
        });            

        
      }
      redirect(subsetid,pid);

    }
  }else{
      //get the answer of the examinee
      var answer = $('.ans').map( function() {
          name = $(this).text(); 
          return name;
      }).get();
      //get the correct answer
      var corranswer = $('.corrans').map( function() {
          txt = $(this).text();
          return txt;
      }).get();
      //get question id
      var qids = $('.page').map( function() {
          txt = $(this).attr('id');
          return txt.substring(txt.indexOf('q')+1);
      }).get();

      for(x=0;x<corranswer.length;x++){
          qid = qids[x];
          if(corranswer[x].toUpperCase()==answer[x].toUpperCase()){
            response = "CORRECT";
          }else{
            response = "WRONG";
          }
        strdata = 'qid=' + qid + '&response=' + response + '&answer=' + answer[x]
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=FILLIN';

        $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){
                }   
                    
        });            

        
      }
      redirect(subsetid,pid);
 
  }
}

//submit agree disagree answer
function submitadexam(subsetid,pid,stat){
  if(stat=="CLICK"){
    if(confirm('Once submitted it cannot be undone!')){
      //get the answer of the examinee
      var answer = $('.ans').map( function() {
          name = $(this).text(); 
          return name;
      }).get();
      //get the correct answer
      var corranswer = $('.corrans').map( function() {
          txt = $(this).text();
          return txt;
      }).get();
      //get question id
      var qids = $('.page').map( function() {
          txt = $(this).attr('id');
          return txt.substring(txt.indexOf('q')+1);
      }).get();

      for(x=0;x<corranswer.length;x++){
          qid = qids[x];
        strdata = 'qid=' + qid + '&answer=' + answer[x]
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=AGREEDISAGREE';

        $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){
                }   
                    
        }); 

        
      }
      redirect(subsetid,pid);

    }
  }else{
      //get the answer of the examinee
      var answer = $('.ans').map( function() {
          name = $(this).text(); 
          return name;
      }).get();
      //get the correct answer
      var corranswer = $('.corrans').map( function() {
          txt = $(this).text();
          return txt;
      }).get();
      //get question id
      var qids = $('.page').map( function() {
          txt = $(this).attr('id');
          return txt.substring(txt.indexOf('q')+1);
      }).get();

      for(x=0;x<corranswer.length;x++){
          qid = qids[x];
        strdata = 'qid=' + qid + '&answer=' + answer[x]
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=AGREEDISAGREE';
                    
        $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){
                }   
                    
        }); 

        
      }
      redirect(subsetid,pid);
  }

}

//redirect to desired page
function redirect(sid,pid){

    $.ajax({
        type: "POST",
        url: "proc/updateexam.php",
        data: 'subsetid=' + sid + '&pid=' + pid,
          success: function(result){
            if(result=='success'){

                window.opener.location.href='profile.php';
                window.close();
            }else if(result=='finished'){
                window.opener.location.href='index.php';
                window.location='examdone.php';

            }

          }   
                    
    }); 

 
  //if(currentval==lastexam){
  //    window.opener.location.href="index.php";
  //    window.location="examdone.php";



}

//show question 
function showq(qid,sid){

  $('.page').removeClass('active');
  $('#q'+qid).addClass('active');

  $('#q_cont').load('proc/showquestion.php?sid='+sid + '&qid='+ qid);
  qans=$('#rbnval'+qid).text();
  
  

  
var myvar= setInterval(function(){
    //multiple choice
    $('input[name="subid-'+qid+'"][value="'+qans+'"]').prop("checked",true);
    //fill in the blank
    $('#fitbans-'+qid).val(qans);
    //agree and disagree
    $('input[name="adsubid-'+qid+'"][value="'+qans+'"]').prop("checked",true);
    clearInterval(myvar);
  }, 500);
}


//marked answered question in fill in the blank
function markans(qid,txt){
  if($.trim(txt)==''){
    $('#b'+qid).empty();
    $('#rbnval'+qid).text(txt);
    $('#ans'+qid).text(txt);
  }else{
    $('#b'+qid).html('<span class="glyphicon glyphicon-ok"></span>');
    $('#rbnval'+qid).text(txt);
    $('#ans'+qid).text(txt);
  }
}