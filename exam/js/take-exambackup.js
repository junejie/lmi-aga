var ctr=0;
$(document).ready(function(){
  function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
  $(document).on("keydown", disableF5);
  
  //$('#btn'+first).prop("disabled",false);
  //$('.ss').click(function(){
      //$(this).prop("disabled",true);
      //$('#'+btnarray[ctr+=1]).prop("disabled",false);
  //})

  $('.rbn-choices').click(function(){
    cont = $(this).attr('name');
    answer=$(this).val();
    qid = cont.substring(cont.indexOf('-')+1);
    $('#ans'+qid).text(answer);
  })
  
})

function loadquestion(subsetid,timelimit){
	 //myWindow = window.open("proc/question.php?subsetid="+subsetid, "", "width=800, height=500");
   
     popupwindow("question.php?sid="+subsetid+"&timelimit="+timelimit, "", '900', '500');

}

function popupwindow(url, title, w, h) {
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, "", 'toolbar=no, location=no, directories=no, status=0, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

function countdown(){

		m=30;
		s=60;

	var interval = setInterval(function() {
		
    	s--;
    	$('#tmr').text(m+':'+s);

    	// Display 'counter' wherever you want to display it.
    	if (s <= 0) {
        	m--;
        	s=60;
        	 
    	}
	}, 1000);
}

//start exam
function startexam(subsetid,timelimit){
  
    window.location="loadquestion.php?sid="+subsetid+"&timelimit="+timelimit;
}


 
function getans(val){
  alert(val);
}

//submit multiple choice answers
function submitmcexam(subsetid,pid,stat){
  if(stat=="CLICK"){
    if(confirm('Once submitted it cannot be undone!')){
        var levelArray = $('.mcq').map( function() {
            name = $(this).attr('id');            
            return name;
        }).get();

         var correctanswer = $('.mcqans').map( function() {
            an = $(this).text();
            return an; 
        }).get();

         
        for(x=0;x<levelArray.length;x++){
          qid = levelArray[x];
          if($('input[name=subid-'+levelArray[x]+']:radio:checked').val()==undefined){
            answer ='';
          }else{
            answer = $('input[name=subid-'+levelArray[x]+']:radio:checked').val();
          }

          if(correctanswer[x].toUpperCase() == answer.toUpperCase()){
            response = 'CORRECT';
          }else{
             response = 'WRONG';
          }
          

          strdata = 'qid=' + qid + '&response=' + response + '&answer=' + answer 
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=MULTIPLE';
          

          $.ajax({
                    type: "POST",
                    url: "proc/getanswer.php",
                    data: strdata,
                    success: function(result){ 
                    

                    }   
                      
                });
        };

        redirect(subsetid,lastsubsetid);
    }
  }else{
        var levelArray = $('.mcq').map( function() {
            name = $(this).attr('id');
            return name;
        }).get();

         var correctanswer = $('.mcqans').map( function() {
            an = $(this).text();
            return an; 
        }).get();

         
        for(x=0;x<levelArray.length;x++){
          qid = levelArray[x];
          if($('input[name=subid-'+levelArray[x]+']:radio:checked').val()==undefined){
            answer ='';
          }else{
            answer = $('input[name=subid-'+levelArray[x]+']:radio:checked').val();
          }

          if(correctanswer[x].toUpperCase() == answer.toUpperCase()){
            response = 'CORRECT';
          }else{
             response = 'WRONG';
          }
          

          strdata = 'qid=' + qid + '&response=' + response + '&answer=' + answer 
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=MULTIPLE';
          

          $.ajax({
                    type: "POST",
                    url: "proc/getanswer.php",
                    data: strdata,
                    success: function(result){ 
                    

                    }   
                      
                });
        };

        redirect(subsetid,lastsubsetid);
  }

 
}

//submit fillin answer 
function submitfitbexam(subsetid,pid,stat){
if(stat=="CLICK"){
  if(confirm('Once submitted it cannot be undone!')){
      var fitbq = $('.fitbq').map( function() {
          name = $(this).attr('id');
          //selected = $('input[name='+name+']:radio').val();
           
          return name;
      }).get();

      var corr = $('.fitbqans').map( function() {
          ca = $(this).text();
          //selected = $('input[name='+name+']:radio').val();
          return ca; 
      }).get();

      for(x=0;x<fitbq.length;x++){
        qid = fitbq[x];
        if($.trim($('#fitbans-'+fitbq[x]).val())==''){
          answer = '';
        }else{
          answer = $('#fitbans-'+fitbq[x]).val();
        }
        
        if(corr[x].toUpperCase()==answer.toUpperCase()){
          response = "CORRECT";
        }else{
          response = "WRONG";
        }
        
        strdata = 'qid=' + qid + '&response=' + response + '&answer=' + answer
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=FILLIN';

                

        $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){ 
                  

                }   
                    
              });

      }

      redirect(subsetid,lastsubsetid);
  }
}else{
      var fitbq = $('.fitbq').map( function() {
          name = $(this).attr('id');
          //selected = $('input[name='+name+']:radio').val();
           
          return name;
      }).get();

      var corr = $('.fitbqans').map( function() {
          ca = $(this).text();
          //selected = $('input[name='+name+']:radio').val();
          return ca; 
      }).get();

      for(x=0;x<fitbq.length;x++){
        qid = fitbq[x];
        if($.trim($('#fitbans-'+fitbq[x]).val())==''){
          answer = '';
        }else{
          answer = $('#fitbans-'+fitbq[x]).val();
        }
        
        if(corr[x].toUpperCase()==answer.toUpperCase()){
          response = "CORRECT";
        }else{
          response = "WRONG";
        }
        
        strdata = 'qid=' + qid + '&response=' + response + '&answer=' + answer
                    + '&pid=' + pid + '&subsetid=' + subsetid + '&tpe=FILLIN';

                

        $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){ 
                  

                }   
                    
              });

      }

      redirect(subsetid,lastsubsetid);

}

}

//submit agree disagree answer
function submitadexam(subsetid,pid,stat){
if(stat=="CLICK"){
  if(confirm('Once submitted it cannot be undone!')){
      var adq = $('.adq').map( function() {
        name = $(this).attr('id');
        return name;
      }).get();

      for(x=0;x<adq.length;x++){
        qid = adq[x];
        if($('input[name=ad-subid'+qid+']:radio:checked').val()==undefined){
          answer ='';
        }else{
          answer = $('input[name=ad-subid'+qid+']:radio:checked').val();
        }

        strdata = 'qid=' + qid + '&answer=' + answer + '&pid=' + pid 
                    + '&subsetid=' + subsetid + '&tpe=AGREEDISAGREE';
          $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){ 
                  alert(result);

                }   
                    
              });            
      }

      redirect(subsetid,lastsubsetid);
  }
}else{
      var adq = $('.adq').map( function() {
        name = $(this).attr('id');
        return name;
      }).get();

      for(x=0;x<adq.length;x++){
        qid = adq[x];
        if($('input[name=ad-subid'+qid+']:radio:checked').val()==undefined){
          answer ='';
        }else{
          answer = $('input[name=ad-subid'+qid+']:radio:checked').val();
        }

        strdata = 'qid=' + qid + '&answer=' + answer + '&pid=' + pid 
                    + '&subsetid=' + subsetid + '&tpe=AGREEDISAGREE';
          $.ajax({
                type: "POST",
                url: "proc/getanswer.php",
                data: strdata,
                success: function(result){ 
                  alert(result);

                }   
                    
              });            
      }

      redirect(subsetid,lastsubsetid);
}

}

function redirect(currentval,lastexam){
  if(currentval==lastexam){
    window.opener.location.href="index.php";
    window.location="examdone.php";
  }else{
    window.close();
  }
  //if(currentval==lastexam){
  //    window.opener.location.href="index.php";
  //    window.location="examdone.php";



}

//show question 
function showq(qid,sid){

  $('.page').removeClass('active');
  $('#q'+qid).addClass('active');
  $('#q_cont').load('proc/showquestion.php?sid='+sid + '&qid='+ qid);

}