$(document).ready(function(){

			//Save click
			$('button#btnSave').click(function(){
			removeValidation();
				
				if($('input[name=position]').val() == '' || $('select[name=category]').val() == '' || $('input[name=exam-name]').val() == '' || $('input[name=set]').val() == '' || $('iinput[name=type]').val() == '' || $('input[name=exam-time]').val() == ''){
					
					if($('input[name=position]').val() == ''){

						validationClass('[name=position]','has-error');
						$('input[name=position]').focus();

					}else{
					
						validationClass('[name=position]','has-success', '');
					
					}

					if($('select[name=category]').val() == ''){
                
						validationClass('[name=category]','has-error');
						$('select[name=category]').focus();
            
					}else{
					
						validationClass('[name=category]','has-success');
					
					}
					
					if($('input[name=type]').val() == ''){
                
						validationClass('[name=type]','has-error');
						$('input[name=type]').focus();
            
					}else{
					
						validationClass('[name=type]','has-success');
					
					}

					if($('input[name=exam-name]').val() == ''){

						validationClass('[name=exam-name]','has-error');
						$('input[name=exam-name]').focus();

					}else{
					
						validationClass('[name=exam-name]','has-success', '');
					
					}

					if($('input[name=set]').val() == ''){

						validationClass('[name=set]','has-error');
						$('input[name=set]').focus();

					}else{
					
						validationClass('[name=set]','has-success', '');
					
					}

					if($('input[name=exam-time]').val() == ''){

						validationClass('[name=exam-time]','has-error');
						$('input[name=exam-time]').focus();

					}else{
					
						validationClass('[name=exam-time]','has-success', '');
					
					}
			
				}else{

					setoftest = $('input[name=set]').val();
					position = $('input[name=position]').val();
					examtype = $('input[name=type]').val();
					examname = $('input[name=exam-name]').val();
					category = $('select[name=category]').val();
					timelimit = $('input[name=exam-time]').val();

					$.ajax({
							type: "POST",
							url: "proc/process-add-exam-type.php",
							data: "position="+position+"&examtype="+examtype+"&examname="+examname+"&category="+category+"&timelimit="+timelimit+"&setoftest="+setoftest,
							success: function(html){    
								if(html=='true'){
									modalSuc();
									clearall();
									modalRes();
								}else{
									alert('something went wrong!');
									alert(html);
								}
							},
							beforeSend:function(){
								modalLoad();
							}
					});
				
				}
				
			});


			/*
			EFFECT FUNCTIONS
			*/
			
			//Modal Loading

			function clearall(){
				$('input[name=set]').val('');
				$('input[name=position]').val('');
				$('input[name=type]').val('');
				$('input[name=exam-name]').val('');
				$('select[name=category]').val('');
				$('input[name=exam-time]').val('');
			}

			function modalLoad(){
				$('#modal-alert').modal('show');//show loading modal
			}
			
			//Modal Suc
			function modalSuc(){
				setTimeout(function() { 
					$('img#loading').hide();
					$('h1#loading-text').text('Done!');
			
					setTimeout(function() { 
						$('h1#loading-text').text('Done!');
						$('#modal-alert').modal('hide'); 
						$('tbody#cassette-items').load('proc/get-cassette-info.php');
					
					}, 700);//hide loading modal
				
				}, 1000);//hide loading modal
			}

			function modalRes(){
				$('img#loading').show();
				$('h1#loading-text').text('Saving...');
				$('p.help-block').remove();

			}
			
			//Function for validation
            function validationClass(name,validationClass){
			
			//function validationClass(name,validationClass,validationError){
                $(name).parents('div[class^="form-group"]').addClass(validationClass);

				/*
				if(validationError != ''){
					$(name).after('<p class="help-block">'+ validationError +'</p>');
				}
				*/
			}

            function removeValidation(){
                //$('p.help-block').remove();
                $('div.form-group').removeClass('has-error has-success');
            }


});