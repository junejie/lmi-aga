<?php
  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<!-- codes starts here -->
<h1><i class="nav-icons fa fa-home"></i>Subjects <small>Module</small></h1>
<div class="table-responsive">
      <table class="table table-condensed">
        <thead>
          <tr>
            <th>Subjects</th>
          </tr>
        </thead>
        <tbody id="tableResult">
          <!-- insert members -->
          <?php include_once('proc/process-view-students-cur.php')

          ?>
        </tbody>
      </table>
    </div>

<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

  <script src="js/scripts-manage-curriculum.js"></script>
  </body>
</html>