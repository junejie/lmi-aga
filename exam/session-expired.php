<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Session Expired</title>
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="dist/flat/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/error.css" rel="stylesheet">
  </head>

  <body>
    <div class="container">
        <div class="big-error jumbotron text-center">
          <h1><i class="big-glyp glyphicon glyphicon-remove"></i></h1>
          <h1>Session Expired</h1>
        </div>

    </div> <!-- /container -->
  </body>
</html>
