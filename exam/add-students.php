<?php
  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<style type="text/css">
    .modal-dialog {
        width: 900px;
        margin: 20px auto;
    }
</style>
<!-- codes starts here -->
<h1>
    <i class="nav-icons fa fa-home">
    </i>Students 
<small>Add</small>
</h1>

          <!-- edit modal -->
         <form id="user-edit-form" class="form-horizontal" role="form">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="edit-user-label">STD-<?php echo rand(1000,9999); ?></h4>
                </div>
                <div class="modal-body row">

                  <div class="row col-md-12">
                    <div class="col-md-5">
                      <!-- Your first column here -->
                      <img src="./img/fff.gif" class="center-block img-responsive img-circle">
                    </div>

                    <div class="col-md-7">
                      <div class="row">
                        <p id="modal-type" class="lead text-muted">Full Name</p>

                          <div class="col-sm-4">
                            <div class="form-group">
                              <input id="#modal-edit-first" type="text" class="form-control" placeholder="First Name" name="firstname" />
                            </div>
                          </div> 

                          <div class="col-sm-4"> 
                            <div class="form-group">
                              <input id="#modal-edit-mid" type="text" class="form-control" placeholder="Middle Name" name="midname" />
                            </div>
                          </div>

                          <div class="col-sm-4">
                            <div class="form-group">
                              <input id="#modal-edit-last" type="text" class="form-control" placeholder="Last Name" name="lastname" />
                            </div>
                          </div>
                        
                      </div>

                      <div class="row">
                        <div class="col-md-7">
                        <div class="form-group">
                          <span class="lead text-muted">Gender</span><br/><br/>
                          <select class="form-control" name="gender">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                          </select>
                        </div>
                      </div>         
                      </div>

                      <div class="row">
                          <p id="modal-type" class="lead text-muted">Birthday</p>

                          <div class="col-sm-7">
                            <div class="form-group">
                              <input id="#modal-edit-birth" type="date" class="form-control" placeholder="Birthday" name="bday" />
                            </div>
                          </div>        
                      </div>

      
                    </div>

                  </div>

                  <div class="row col-sm-12">
                    <hr />
                        <span class="lead text-muted">Address</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-address" type="text" class="form-control" placeholder="Address" name="address" />
                          </div>
                        </div>
                        <span class="lead text-muted">Email</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-email" type="text" class="form-control" placeholder="Email" name="email" />
                          </div>
                        </div>
                        <span class="lead text-muted">Contact</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-contact" type="text" class="form-control" placeholder="Contact #" name="contact" />
                          </div>
                        </div>
                        <span class="lead text-muted">Password</span>
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="#modal-edit-password" type="password" class="form-control" placeholder="Password" name="password" />
                          </div>
                        </div>

                        <input id="#modal-id" type="text" class="form-control" name="userid" style="display:none;" />
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-success">Add</button>
                </div>
              </div>
            </div>
          </form>
        <!-- edit modal -->

        

<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

  <script src="js/scripts-add-student-users.js"></script>
  </body>
</html>