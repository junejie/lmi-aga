<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Automated Psychological Testing System</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Automated Psychological Testing System</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Examination</a></li>
            <!--
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            -->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">

      <div class="content">

        <div class="row">
          
          <div class="col-lg-8">
            <h1>Registration Form</h1>
            

            <div class="panel panel-default">
              <div class="panel-body">


                <ul class="nav nav-pills nav-wizard">
                  <li class="active"><a href="#" data-toggle="tab">Data Information</a><div class="nav-arrow"></div></li>
                  <li><div class="nav-wedge disabled"></div><a href="#" class="nav-item" value="test1">Mental Test</a><div class="nav-arrow"></div></li>
                  <li><div class="nav-wedge disabled"></div><a href="#" class="nav-item">Aptitude Test</a><div class="nav-arrow"></div></li>
                  <li><div class="nav-wedge disabled"></div><a href="#" class="nav-item">Logic Test</a><div class="nav-arrow"></div></li>
                  <li><div class="nav-wedge disabled"></div><a href="#" class="nav-item">Well Done!</a></li>
                </ul>

                <div id="reg-items" class="row"><!-- Registration Items -->

                  <div id="data-info"><!-- Item 1 -->
                      <div class="col-md-12">
                        <h3>Information Form</h3>
                        <div class="alert alert-info" role="alert">Instructions here</div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group">
                          <label>Username:</label>
                          <input type="text" class="form-control" name="username" />
                        </div>
                        
                        <div class="form-group">
                          <label>Password:</label>
                          <input type="password" class="form-control" name="password" />
                        </div>

                        <div class="form-group">
                          <label>Firstname:</label>
                          <input type="text" class="form-control" name="firstname" />
                        </div>
                        
                        <div class="form-group">
                          <label>Lastname:</label>
                          <input type="text" class="form-control" name="lastname" />
                        </div>
                        
                        <div class="form-group">
                          <label>Email:</label>
                          <input type="email" class="form-control" name="email" />
                        </div>

                        <div class="form-group">
                          <label>Position:</label>
                          <select class="form-control" name="user-type">
                            <option></option>
                            <option>Position 1</option>
                            <option>Position 2</option>
                            <option>Position 3</option>
                          </select>
                        </div>

                      </div>
                  </div><!-- Item 1 -->

                  <div id="mental-test" class="hide"><!-- Item 2 -->
                      <div class="col-md-12">
                        <h3>Mental Test</h3>
                        <div class="alert alert-info" role="alert">Instructions here</div>
                      </div>
                      <div class="col-md-12">

                        <div class="form-group">
                          <label>1. Sample question only?</label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Answer 1
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Answer 2
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 3
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 4
                            </label>
                          </div>
                        </div>
                        <br />

                        <div class="form-group">
                          <label>2. Sample question only?</label>
                          <div class="col-md-12">
                            <div class="radio">
                             <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Wow this is very super duper long answer sample 1
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Wow this is very super duper long answer sample 2
                              </label>
                            </div>
                            <div class="radio disabled">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                Wow this is very super duper long answer sample 3
                              </label>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label>3. Sample question only?</label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Answer 1
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Answer 2
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 3
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 4
                            </label>
                          </div>
                        </div>
                        <br />

                        <div class="form-group">
                          <label>4. Sample question only?</label>
                          <div class="col-md-12">
                            <div class="radio">
                             <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Wow this is very super duper long answer sample 1
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Wow this is very super duper long answer sample 2
                              </label>
                            </div>
                            <div class="radio disabled">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                Wow this is very super duper long answer sample 3
                              </label>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label>5. Sample question only?</label>
                          <div class="col-md-12">
                            <div class="radio">
                             <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Wow this is very super duper long answer sample 1
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Wow this is very super duper long answer sample 2
                              </label>
                            </div>
                            <div class="radio disabled">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                Wow this is very super duper long answer sample 3
                              </label>
                            </div>
                          </div>
                        </div>

                      </div>
                  </div><!-- Item 2 -->

                  <div id="aptitude-test" class="hide"><!-- Item 3 -->
                      <div class="col-md-12">
                        <h3>Aptitude Test</h3>
                        <div class="alert alert-info" role="alert">Instructions here</div>
                      </div>
                      <div class="col-md-12">

                        <div class="form-group">
                          <label>1. Sample question only?</label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Answer 1
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Answer 2
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 3
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 4
                            </label>
                          </div>
                        </div>
                        <br />

                        <div class="form-group">
                          <label>2. Sample question only?</label>
                          <div class="col-md-12">
                            <div class="radio">
                             <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Wow this is very super duper long answer sample 1
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Wow this is very super duper long answer sample 2
                              </label>
                            </div>
                            <div class="radio disabled">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                Wow this is very super duper long answer sample 3
                              </label>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label>3. Sample question only?</label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Answer 1
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Answer 2
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 3
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 4
                            </label>
                          </div>
                        </div>
                        <br />

                        <div class="form-group">
                          <label>4. Sample question only?</label>
                          <div class="col-md-12">
                            <div class="radio">
                             <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Wow this is very super duper long answer sample 1
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Wow this is very super duper long answer sample 2
                              </label>
                            </div>
                            <div class="radio disabled">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                Wow this is very super duper long answer sample 3
                              </label>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label>5. Sample question only?</label>
                          <div class="col-md-12">
                            <div class="radio">
                             <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Wow this is very super duper long answer sample 1
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Wow this is very super duper long answer sample 2
                              </label>
                            </div>
                            <div class="radio disabled">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                Wow this is very super duper long answer sample 3
                              </label>
                            </div>
                          </div>
                        </div>

                      </div>
                  </div><!-- Item 3 -->

                  <div id="logic-test" class="hide"><!-- Item 4 -->
                      <div class="col-md-12">
                        <h3>Logic Test</h3>
                        <div class="alert alert-info" role="alert">Instructions here</div>
                      </div>
                      <div class="col-md-12">

                        <div class="form-group">
                          <label>1. Sample question only?</label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Answer 1
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Answer 2
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 3
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 4
                            </label>
                          </div>
                        </div>
                        <br />

                        <div class="form-group">
                          <label>2. Sample question only?</label>
                          <div class="col-md-12">
                            <div class="radio">
                             <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Wow this is very super duper long answer sample 1
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Wow this is very super duper long answer sample 2
                              </label>
                            </div>
                            <div class="radio disabled">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                Wow this is very super duper long answer sample 3
                              </label>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label>3. Sample question only?</label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Answer 1
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Answer 2
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 3
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Answer 4
                            </label>
                          </div>
                        </div>
                        <br />

                        <div class="form-group">
                          <label>4. Sample question only?</label>
                          <div class="col-md-12">
                            <div class="radio">
                             <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Wow this is very super duper long answer sample 1
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Wow this is very super duper long answer sample 2
                              </label>
                            </div>
                            <div class="radio disabled">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                Wow this is very super duper long answer sample 3
                              </label>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label>5. Sample question only?</label>
                          <div class="col-md-12">
                            <div class="radio">
                             <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Wow this is very super duper long answer sample 1
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Wow this is very super duper long answer sample 2
                              </label>
                            </div>
                            <div class="radio disabled">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                Wow this is very super duper long answer sample 3
                              </label>
                            </div>
                          </div>
                        </div>

                      </div>
                  </div><!-- Item 4 -->

                  <div id="finished-form" class="hide"><!-- Item 5 -->
                      <div class="col-md-12">
                        <h1>Well Done!</h1>
                        <h3>Congratulations!</h3>
                      </div>
                  </div><!-- Item 5 -->

                </div><!-- Registration Items -->

                
              </div>
              <div class="panel-footer">
                <div class="row">
                  <div class="col-md-12">
                    <button id="btnNext" type="button" class="btn btn-default btn-sm pull-right">Next</button>
                  </div>
                </div>
              </div>
            </div>


          </div>

        </div>
        
      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../dist/jquery/jquery.min.js"></script>
    <script src="../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
          var counter = 0;

          $('#btnNext').click(function(){

              $('ul.nav-wizard li.active').next().addClass('active');
              $('ul.nav-wizard li.active').prev().removeClass('active');
              
              showForm(++counter);//increment to view next page

          });


          function showForm(formNo){

            switch(formNo) {
              case 1:
                  $('#data-info').addClass('hide');
                  $('#mental-test').removeClass('hide');
                  break;
              case 2:
                  $('#mental-test').addClass('hide');
                  $('#aptitude-test').removeClass('hide');
                  break;
              case 3:
                  $('#aptitude-test').addClass('hide');
                  $('#logic-test').removeClass('hide');
                  break;
              case 4:
                  $('#logic-test').addClass('hide');
                  $('#finished-form').removeClass('hide');
                  break;
            } 

          }
      });
    </script>


  </body>
</html>
