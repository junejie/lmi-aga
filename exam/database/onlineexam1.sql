-- phpMyAdmin SQL Dump
-- version 3.5.3-rc1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 03, 2015 at 02:41 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onlineexam1`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

DROP TABLE IF EXISTS `announcement`;
CREATE TABLE IF NOT EXISTS `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `announceto` char(50) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `title`, `date`, `announceto`, `content`) VALUES
(6, 'Long Week', '2015-12-31', 'All', '<p>Long now</p>'),
(8, 'Long Month', '2015-04-03', 'Grade 1', '<p>Grade 1 vv</p>'),
(9, 'Summer Vacation', '2015-04-16', 'All', '<p>summer will start</p>'),
(10, 'For kinder only A-001', '2015-04-10', 'Kinder', '<p>This is for kinder only</p>');

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qid` int(10) unsigned NOT NULL,
  `response` varchar(10) NOT NULL,
  `answer` varchar(50) NOT NULL,
  `profiling_id` varchar(20) NOT NULL,
  `examsubset_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `qid`, `response`, `answer`, `profiling_id`, `examsubset_id`) VALUES
(1, 78, 'CORRECT', 'True', 'STD-6770', 1),
(2, 78, 'CORRECT', 'True', 'STD-3482', 1),
(3, 78, 'CORRECT', 'True', 'STD-6490', 1),
(4, 78, 'CORRECT', 'True', 'STD-6490', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category`) VALUES
(1, 'Mental Ability'),
(2, 'Aptitude'),
(3, 'Personality');

-- --------------------------------------------------------

--
-- Table structure for table `curic`
--

DROP TABLE IF EXISTS `curic`;
CREATE TABLE IF NOT EXISTS `curic` (
  `ID` varchar(250) NOT NULL,
  `desc` varchar(250) NOT NULL,
  `subjects` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `curic`
--

INSERT INTO `curic` (`ID`, `desc`, `subjects`) VALUES
('CUR-3', '', 'TRIGO2 GRADE2,PROGRAMMING GRADE2,PE GRADE2,'),
('CUR 2015', '', 'ENG1 GRADE1,MATH1 GRADE1,TRIGO1 GRADE1,PROGRAMMING GRADE1,PE GRADE1,ENG2 GRADE2,MATH2 GRADE2,TRIGO2 GRADE2,PROGRAMMING GRADE2,PE GRADE2,'),
('High School', '', ''),
('Elementary', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `evaluation`
--

DROP TABLE IF EXISTS `evaluation`;
CREATE TABLE IF NOT EXISTS `evaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionno` int(50) NOT NULL,
  `answer` int(10) NOT NULL,
  `studentid` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `evaluationresult`
--

DROP TABLE IF EXISTS `evaluationresult`;
CREATE TABLE IF NOT EXISTS `evaluationresult` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qno` int(50) NOT NULL,
  `answer` int(11) NOT NULL,
  `teacherid` varchar(20) NOT NULL,
  `studentid` varchar(20) NOT NULL,
  `subject` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=201 ;

--
-- Dumping data for table `evaluationresult`
--

INSERT INTO `evaluationresult` (`id`, `qno`, `answer`, `teacherid`, `studentid`, `subject`) VALUES
(101, 1, 3, 'TCH-1234', 'STD-3349', 'MATH'),
(102, 2, 1, 'TCH-1234', 'STD-3349', 'MATH'),
(103, 3, 1, 'TCH-1234', 'STD-3349', 'MATH'),
(104, 4, 2, 'TCH-1234', 'STD-3349', 'MATH'),
(105, 5, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(106, 6, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(107, 7, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(108, 8, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(109, 9, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(110, 10, 3, 'TCH-1234', 'STD-3349', 'MATH'),
(111, 11, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(112, 12, 3, 'TCH-1234', 'STD-3349', 'MATH'),
(113, 13, 1, 'TCH-1234', 'STD-3349', 'MATH'),
(114, 14, 2, 'TCH-1234', 'STD-3349', 'MATH'),
(115, 15, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(116, 16, 3, 'TCH-1234', 'STD-3349', 'MATH'),
(117, 17, 3, 'TCH-1234', 'STD-3349', 'MATH'),
(118, 18, 3, 'TCH-1234', 'STD-3349', 'MATH'),
(119, 19, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(120, 20, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(121, 21, 3, 'TCH-1234', 'STD-3349', 'MATH'),
(122, 22, 1, 'TCH-1234', 'STD-3349', 'MATH'),
(123, 23, 1, 'TCH-1234', 'STD-3349', 'MATH'),
(124, 24, 1, 'TCH-1234', 'STD-3349', 'MATH'),
(125, 25, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(126, 26, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(127, 27, 1, 'TCH-1234', 'STD-3349', 'MATH'),
(128, 28, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(129, 29, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(130, 30, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(131, 31, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(132, 32, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(133, 33, 5, 'TCH-1234', 'STD-3349', 'MATH'),
(134, 34, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(135, 35, 3, 'TCH-1234', 'STD-3349', 'MATH'),
(136, 36, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(137, 37, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(138, 38, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(139, 39, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(140, 40, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(141, 41, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(142, 42, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(143, 43, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(144, 44, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(145, 45, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(146, 46, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(147, 47, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(148, 48, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(149, 49, 4, 'TCH-1234', 'STD-3349', 'MATH'),
(150, 50, 3, 'TCH-1234', 'STD-3349', 'MATH'),
(151, 1, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(152, 2, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(153, 3, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(154, 4, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(155, 5, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(156, 6, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(157, 7, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(158, 8, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(159, 9, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(160, 10, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(161, 11, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(162, 12, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(163, 13, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(164, 14, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(165, 15, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(166, 16, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(167, 17, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(168, 18, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(169, 19, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(170, 20, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(171, 21, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(172, 22, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(173, 23, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(174, 24, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(175, 25, 2, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(176, 26, 2, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(177, 27, 2, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(178, 28, 2, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(179, 29, 2, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(180, 30, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(181, 31, 2, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(182, 32, 2, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(183, 33, 3, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(184, 34, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(185, 35, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(186, 36, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(187, 37, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(188, 38, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(189, 39, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(190, 40, 5, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(191, 41, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(192, 42, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(193, 43, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(194, 44, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(195, 45, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(196, 46, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(197, 47, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(198, 48, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(199, 49, 4, 'TCH-1234', 'STD-3349', 'SCIENCE'),
(200, 50, 4, 'TCH-1234', 'STD-3349', 'SCIENCE');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

DROP TABLE IF EXISTS `exams`;
CREATE TABLE IF NOT EXISTS `exams` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `situation` longtext NOT NULL,
  `questionno` int(100) NOT NULL,
  `question` varchar(255) NOT NULL,
  `choices` longtext NOT NULL,
  `answer` varchar(50) NOT NULL,
  `examname` longtext NOT NULL,
  `setoftest` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `situation`, `questionno`, `question`, `choices`, `answer`, `examname`, `setoftest`) VALUES
(1, '', 5, 'who are you', 'a\nb\nc\nd', 'imgquestion/TPhoto_00003.jpg', 'MDQ', ''),
(2, '', 3, 'imgquestion/TPhoto_00001.jpg', 'a\nb\nc\nd', 'imgquestion/TPhoto_00003.jpg', 'WATSON GLASER CRITICAL THINKING', 'INTERFERENCE'),
(3, '', 10, 'imgquestion/TPhoto_00004.jpg', 'a\nb\nc\nd', 'imgquestion/TPhoto_00006.jpg', 'MANAGEMENT DEVELOPMENT QUESTIONARE', ''),
(4, '', 23, 'imgquestion/Lighthouse.jpg', 'a\nb\nc\nd', 'imgquestion/Tulips.jpg', 'WATSON GLASER CRITICAL THINKING', 'INTERFERENCE');

-- --------------------------------------------------------

--
-- Table structure for table `examsubset`
--

DROP TABLE IF EXISTS `examsubset`;
CREATE TABLE IF NOT EXISTS `examsubset` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `typeoftest_id` int(10) NOT NULL,
  `examsubsetname` varchar(255) NOT NULL,
  `timelimit` varchar(6) NOT NULL,
  `typeofanswer` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `examsubset`
--

INSERT INTO `examsubset` (`id`, `typeoftest_id`, `examsubsetname`, `timelimit`, `typeofanswer`) VALUES
(1, 1, 'TEST I: VOCABULARY', '30', 'MULTIPLE CHOICE'),
(2, 1, 'TEST II: ANALOGY', '30', 'MULTIPLE CHOICE'),
(3, 1, 'TEST III: NUMERICAL ABILITY', '30', 'MULTIPLE CHOICE'),
(4, 1, 'TEST IV: NON-VERBAL ABILITY TEST', '30', 'Multiple Choice'),
(5, 2, 'Management Development Questionnaire', '30', 'Agree/Disagree'),
(6, 3, 'MD5 Mental Ability', '30', 'Fill-in the Blank'),
(7, 4, 'INFORMATION', '30', 'Multiple Choice'),
(8, 4, 'COMPREHENSION', '30', 'Multiple Choice'),
(9, 4, 'ARITHMETIC', '30', 'Multiple Choice'),
(10, 4, 'SIMILARITIES', '30', 'Multiple Choice'),
(11, 4, 'VOCABULARY', '30', 'Multiple Choice'),
(12, 5, 'Panukat ng Ugaling Pilipino', '30', 'Agree/Disagree'),
(13, 6, 'Purdue Non-language Test - Form B', '30', 'Multiple Choice'),
(14, 7, 'SFPQ Booklet', '30', 'Agree/Disagree'),
(15, 8, 'TEST 1: INFERENCE', '30', 'Agree/Disagree'),
(16, 8, 'TEST 2: RECOGNITION OF ASSUMPTIONS', '30', 'Agree/Disagree'),
(17, 8, 'TEST 3: DEDUCTION', '30', 'Agree/Disagree'),
(18, 8, 'TEST 4: INTERPRETATION', '30', 'Agree/Disagree'),
(19, 8, 'TEST 5: EVALUATION OF ARGUMENTS', '30', 'Agree/Disagree'),
(20, 9, 'TEST 1: INFERENCE', '30', 'Agree/Disagree'),
(21, 9, 'TEST 2: RECOGNITION OF ASSUMPTIONS', '30', 'Agree/Disagree'),
(22, 9, 'TEST 3: DEDUCTION', '30', 'Agree/Disagree'),
(23, 9, 'TEST 4: INTERPRETATION', '30', 'Agree/Disagree'),
(24, 9, 'TEST 5: EVALUATION OF ARGUMENTS', '30', 'Agree/Disagree'),
(25, 10, 'Workplace Skills Survey', '30', 'Agree/Disagree'),
(26, 11, 'Emotional Quotient Survey', '30', 'Agree/Disagree'),
(30, 16, 'SAMPLE SUBSET NAME', '30', 'MULTIPLE CHOICE');

-- --------------------------------------------------------

--
-- Table structure for table `examtaken`
--

DROP TABLE IF EXISTS `examtaken`;
CREATE TABLE IF NOT EXISTS `examtaken` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `examsubset_id` int(10) unsigned NOT NULL,
  `status` varchar(10) NOT NULL,
  `profiling_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `examtype`
--

DROP TABLE IF EXISTS `examtype`;
CREATE TABLE IF NOT EXISTS `examtype` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `typeoftest_id` int(10) NOT NULL,
  `level_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `examtype`
--

INSERT INTO `examtype` (`id`, `category_id`, `typeoftest_id`, `level_id`) VALUES
(1, 1, 8, '1'),
(2, 2, 2, '1'),
(3, 3, 7, '1'),
(4, 1, 9, '2'),
(5, 2, 2, '2'),
(6, 3, 7, '2'),
(7, 1, 3, '3'),
(8, 2, 4, '3'),
(9, 3, 11, '3'),
(10, 1, 1, '4'),
(11, 3, 10, '4'),
(12, 1, 6, '5'),
(13, 3, 5, '5');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` text NOT NULL,
  `filesize` text NOT NULL,
  `fileloc` text NOT NULL,
  `filetype` text NOT NULL,
  `section` text NOT NULL,
  `teachers_id` text NOT NULL,
  `filestatus` char(50) NOT NULL DEFAULT 'unarchive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `filename`, `filesize`, `fileloc`, `filetype`, `section`, `teachers_id`, `filestatus`) VALUES
(2, 'test.txt', '4', '/uploads/test.txt', 'text/plain', 'Our Lady of Angels', 'TCH-1234', 'archive'),
(3, 'test-2.txt', '4', '/uploads/test-2.txt', 'text/plain', 'Our Lady of Grace', 'TCH-1234', 'unarchive'),
(4, 'J.jpg', '3427', '/uploads/J.jpg', 'image/jpeg', 'Our Lady of Angels', 'TCH-1234', 'unarchive');

-- --------------------------------------------------------

--
-- Table structure for table `grade_level`
--

DROP TABLE IF EXISTS `grade_level`;
CREATE TABLE IF NOT EXISTS `grade_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `grade_level`
--

INSERT INTO `grade_level` (`id`, `grade`) VALUES
(1, 'Kinder'),
(2, 'Grade 1'),
(3, 'Grade 2'),
(4, 'Grade 3'),
(5, 'Grade 4'),
(6, 'Grade 5'),
(7, 'Grade 6'),
(8, 'First Year'),
(9, 'Second Year'),
(10, 'Third Year'),
(11, 'Fourth Year');

-- --------------------------------------------------------

--
-- Table structure for table `instruction`
--

DROP TABLE IF EXISTS `instruction`;
CREATE TABLE IF NOT EXISTS `instruction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `examsubset_id` int(10) unsigned NOT NULL,
  `instruction` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `instruction`
--

INSERT INTO `instruction` (`id`, `examsubset_id`, `instruction`) VALUES
(4, 1, '<p>Sample Instruction</p>'),
(10, 2, '<p>sdfsdfaff</p>'),
(11, 1, '<p>qqqq</p>');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

DROP TABLE IF EXISTS `level`;
CREATE TABLE IF NOT EXISTS `level` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `level` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `level`) VALUES
(1, 'MANAGERIAL (JG 16 AND ABOVE)'),
(2, 'SUPERVISORY (JG 14-15)'),
(3, 'PROFESSIONAL/ TECHNICAL (JG 12-13)'),
(4, 'ADMIN/CLERICAL (JG 8-11)'),
(5, 'TRADES (JG7 AND BELOW)'),
(8, 'DATABASE ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `profiling`
--

DROP TABLE IF EXISTS `profiling`;
CREATE TABLE IF NOT EXISTS `profiling` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `age` int(10) unsigned NOT NULL,
  `dateoftest` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `educ_attaintment` varchar(100) NOT NULL,
  `current_employer` varchar(50) NOT NULL,
  `current_position` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  `level_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `qmasterlist`
--

DROP TABLE IF EXISTS `qmasterlist`;
CREATE TABLE IF NOT EXISTS `qmasterlist` (
  `qid` int(10) NOT NULL AUTO_INCREMENT,
  `examsubset_id` int(10) NOT NULL DEFAULT '0',
  `q_type` varchar(20) NOT NULL DEFAULT '1' COMMENT '0 - Direction, 1 - quizz proper',
  `qno` int(3) NOT NULL DEFAULT '0',
  `qtext` text NOT NULL,
  `qimg` text NOT NULL,
  `answer` varchar(200) NOT NULL,
  `a` text NOT NULL,
  `a_label` varchar(1) NOT NULL,
  `a_img` text NOT NULL,
  `b` text NOT NULL,
  `b_label` varchar(1) NOT NULL,
  `b_img` text NOT NULL,
  `c` text NOT NULL,
  `c_label` varchar(1) NOT NULL,
  `c_img` text NOT NULL,
  `d` text NOT NULL,
  `d_label` varchar(1) NOT NULL,
  `d_img` text NOT NULL,
  `e` text NOT NULL,
  `e_label` varchar(1) NOT NULL,
  `e_img` text NOT NULL,
  `subjectid` int(11) NOT NULL,
  PRIMARY KEY (`qid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `qmasterlist`
--

INSERT INTO `qmasterlist` (`qid`, `examsubset_id`, `q_type`, `qno`, `qtext`, `qimg`, `answer`, `a`, `a_label`, `a_img`, `b`, `b_label`, `b_img`, `c`, `c_label`, `c_img`, `d`, `d_label`, `d_img`, `e`, `e_label`, `e_img`, `subjectid`) VALUES
(78, 1, 'MULTIPLE CHOICE', 1, '<p>sdfsdf</p>', 'undefined', 'True', 'True', 'A', 'undefined', 'False', 'B', 'undefined', '', 'C', 'undefined', '', 'D', 'undefined', '', 'E', 'undefined', 1),
(79, 2, 'MULTIPLE CHOICE', 1, '<p>sadfsadf2</p>', 'undefined', 'True', 'True', 'A', 'undefined', 'False', 'B', 'undefined', '', 'C', 'undefined', '', 'D', 'undefined', '', 'E', 'undefined', 2);

-- --------------------------------------------------------

--
-- Table structure for table `schoolyear`
--

DROP TABLE IF EXISTS `schoolyear`;
CREATE TABLE IF NOT EXISTS `schoolyear` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `sy` varchar(250) NOT NULL,
  `cur` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `schoolyear`
--

INSERT INTO `schoolyear` (`ID`, `sy`, `cur`) VALUES
(1, 'HS-2016', 'CUR-1'),
(2, 'HI-101', 'CUR-2'),
(3, '', ''),
(4, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionname` text NOT NULL,
  `curiculum` text NOT NULL,
  `grade` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `sectionname`, `curiculum`, `grade`) VALUES
(17, 'Our Lady of Angels', 'Elementary', 'Kinder'),
(18, 'Our Lady of Grace', 'Elementary', 'Kinder'),
(19, 'Our Lady of Fatima', 'Elementary', 'Grade 1'),
(20, 'Our Lady of Peace', 'Elementary', 'Grade 2'),
(21, 'Our Lady of Salvation', 'Elementary', 'Grade 2'),
(22, 'Our Lady of Mt. Carmel', 'Elementary', 'Grade 3'),
(23, 'Our Lady of Consolation', 'Elementary', 'Grade 3'),
(24, 'Our Lady of Light', 'Elementary', 'Grade 4'),
(25, 'Our Lady of Guadalupe', 'Elementary', 'Grade 4'),
(26, 'Our Lady of Holy Rosary', 'Elementary', 'Grade 5'),
(27, 'Our Lady of Good Counsel', 'Elementary', 'Grade 5'),
(29, 'Our Lady of Immaculate Concepcion', 'Elementary', 'Grade 6'),
(30, 'Our Lady of Miraculous Medal', 'Elementary', 'Grade 6'),
(31, 'St. Paul', 'High School', 'First Year'),
(32, 'St. Joseph', 'High School', 'First Year'),
(33, 'St. Peter', 'High School', 'First Year'),
(34, 'St. Pedro Calungsod', 'High School', 'First Year'),
(35, 'St. Francis', 'High School', 'Second Year'),
(36, 'St. Therese', 'High School', 'Second Year'),
(37, 'St. Ezekiel', 'High School', 'Second Year'),
(41, 'St. John', 'High School', 'Third Year'),
(42, 'St. Matthew', 'High School', 'Third Year'),
(43, 'St. Luke', 'High School', 'Third Year'),
(45, 'St. Ambrose', 'High School', 'Fourth Year'),
(46, 'St. Monica', 'High School', 'Fourth Year'),
(47, 'St. Rita', 'High School', 'Fourth Year'),
(48, 'St. Augustine', 'High School', 'Fourth Year');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `ID` varchar(250) NOT NULL,
  `fname` varchar(250) NOT NULL,
  `midname` varchar(260) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `gender` varchar(250) NOT NULL,
  `addr` varchar(250) NOT NULL,
  `contact` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `bday` varchar(250) NOT NULL,
  `picture` varchar(500) NOT NULL,
  `status` varchar(250) NOT NULL,
  `password` varchar(500) NOT NULL,
  `section` text NOT NULL COMMENT 'Section',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`ID`, `fname`, `midname`, `lname`, `gender`, `addr`, `contact`, `email`, `bday`, `picture`, `status`, `password`, `section`) VALUES
('STD-3349', 'james', 'b', 'Bond', 'Male', '1', '55', '1@1.ccc', '2014', '1.jpg', 'Active', '111', 'Our Lady of Angels'),
('STD-3482', 'SAMPLE', 'SAMPLE', 'SAMPLE', 'Male', 'SAMPLE', '2344', 'SAMPLE@SSFAS.ASDFDSF', '2015-02-03', '2.png', 'Active', 'SAMPLE', 'Our Lady of Angels'),
('STD-5751', 'manno', 'u lo', 'lo', 'Female', 'brgy. u', '3', 'ww@qq.cc', '44', '1.jpg', 'Active', '111', 'Our Lady of Angels'),
('STD-6490', 'a', 's', 'a', 'Female', 'asdasd', '123', 'asd@asd.com', '2015-02-02', 'Koala.jpg', 'Active', '123', 'Our Lady of Grace'),
('STD-6515', 'ric', 'd', 'wow', 'Male', 'ww', '1', 'ww@qq.cc', '22', '1.jpg', 'Active', '111', 'Our Lady of Grace'),
('STD-6770', 'nix', 'nix', 'nix', 'Male', 'asd', '234324', 'AD@SDFASD.SDAF', '2015-02-18', 'Koala.jpg', 'Active', '123', 'Our Lady of Fatima'),
('STD-7143', 'nica', 'd', 'hui', 'Female', '22', '44', 'e@mailc.cc', '33', '1.jpg', 'Active', '111', 'Our Lady of Fatima');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
CREATE TABLE IF NOT EXISTS `subject` (
  `subjectid` int(11) NOT NULL AUTO_INCREMENT,
  `subjectname` varchar(50) NOT NULL,
  `timelimit` varchar(10) NOT NULL,
  `typeofquestion` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'UNFINISHED',
  PRIMARY KEY (`subjectid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`subjectid`, `subjectname`, `timelimit`, `typeofquestion`, `status`) VALUES
(1, 'Filipino', '30', 'MULTIPLE CHOICE', 'UNFINISHED'),
(2, 'English', '30', 'MULTIPLE CHOICE', 'UNFINISHED');

-- --------------------------------------------------------

--
-- Table structure for table `subjectslist`
--

DROP TABLE IF EXISTS `subjectslist`;
CREATE TABLE IF NOT EXISTS `subjectslist` (
  `ID` int(250) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `grade` text NOT NULL,
  `desc` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subjectslist`
--

INSERT INTO `subjectslist` (`ID`, `name`, `grade`, `desc`) VALUES
(1, 'LANGUAGE', 'Kinder', 'LANGUAGE'),
(2, 'COMMUNICATION', 'Kinder', 'LANGUAGE'),
(3, 'MATH', 'Kinder', 'LANGUAGE'),
(4, 'SCIENCE', 'Kinder', 'LANGUAGE'),
(5, 'STEM', 'Kinder', 'LANGUAGE');

-- --------------------------------------------------------

--
-- Table structure for table `takenevaluation`
--

DROP TABLE IF EXISTS `takenevaluation`;
CREATE TABLE IF NOT EXISTS `takenevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` varchar(20) NOT NULL,
  `teachersid` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'UNFINISHED',
  `subject` text NOT NULL COMMENT 'Subject',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `takenevaluation`
--

INSERT INTO `takenevaluation` (`id`, `studentid`, `teachersid`, `status`, `subject`) VALUES
(1, 'STD-3349', 'TCH-1234', 'FINISHED', 'MATH1 GRADE1'),
(2, 'STD-6770', 'TCH-12345', 'FINISHED', 'PROGRAMMING GRADE2'),
(3, 'STD-3349', 'TCH-1234', 'FINISHED', 'MATH'),
(4, 'STD-3349', 'TCH-1234', 'FINISHED', 'SCIENCE');

-- --------------------------------------------------------

--
-- Table structure for table `tbi_questions`
--

DROP TABLE IF EXISTS `tbi_questions`;
CREATE TABLE IF NOT EXISTS `tbi_questions` (
  `tbi_id` text NOT NULL,
  `tbi_question` text NOT NULL,
  `tbi_answerlist_id` text NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `tbi_questions`
--

INSERT INTO `tbi_questions` (`tbi_id`, `tbi_question`, `tbi_answerlist_id`, `id`) VALUES
('tbi-1', '1.  gives several examples of each concept', 'answer-1', 1),
('tbi-1', '2.  uses concrete, everyday examples to explain concepts and principles', 'answer-1', 2),
('tbi-1', '3.  fails to define new or unfamiliar terms', 'answer-1', 3),
('tbi-1', '4.  uses graphs and diagrams to facilitate explanation.', 'answer-1', 4),
('tbi-1', '5.  repeats difficult ideas several times', 'answer-1', 5),
('tbi-1', '6.  stresses most important points by pausing, speaking slowly, raising voice, etc.', 'answer-1', 6),
('tbi-1', '7.  answers students’ questions thoroughly', 'answer-1', 7),
('tbi-1', '8.  suggests ways of memorizing complicated ideas', 'answer-1', 8),
('tbi-1', '9   writes key terms on blackboard or overhead screen Enthusiasm:    expressive teaching behaviours that communicate instructor enthusiasm and  solicit student attention and interest', 'answer-1', 10),
('tbi-1', '10. speaks in a ''dramatic'' or expressive way', 'answer-1', 11),
('tbi-1', '11. moves about the room while teaching', 'answer-1', 12),
('tbi-1', '12. gestures with head, body, hands, or arms', 'answer-1', 13),
('tbi-1', '13. exhibits facial gestures or expressions', 'answer-1', 14),
('tbi-1', '14. avoids eye contact with students', 'answer-1', 15),
('tbi-1', '15. tells jokes or humorous anecdotes', 'answer-1', 16),
('tbi-1', '16. reads lecture verbatim from prepared notes or text', 'answer-1', 17),
('tbi-1', '17. smiles or laughs while teaching Interaction: teaching behaviours that foster student interaction and class participation', 'answer-1', 19),
('tbi-1', '18. encourages students to ask questions or make comments', 'answer-1', 20),
('tbi-1', '19. praises students for good ideas', 'answer-1', 21),
('tbi-1', '20. asks questions of individual students', 'answer-1', 22),
('tbi-1', '21. asks questions of class as a whole', 'answer-1', 23),
('tbi-1', '22. incorporates student ideas into lecture', 'answer-1', 24),
('tbi-1', '23. uses a variety of media and activities in class', 'answer-1', 25),
('tbi-1', '24. criticizes student when they make errors Organization:  teaching behaviours that serve to structure or organize the subject matter', 'answer-1', 26),
('tbi-1', '25. reviews topics from previous lectures', 'answer-1', 27),
('tbi-1', '26. gives preliminary overview of lecture', 'answer-1', 28),
('tbi-1', '27. puts outline of lecture on blackboard or overhead screen', 'answer-1', 29),
('tbi-1', '28. signals transition from one topic to the next', 'answer-1', 30),
('tbi-1', '29. explains how each topic fits into the course as a whole', 'answer-1', 31),
('tbi-1', '30. periodically summarizes points previously made Pacing: teaching behaviours that affect rate of presentation of course content', 'answer-1', 32),
('tbi-1', '31. dwells excessively on obvious points', 'answer-1', 33),
('tbi-1', '32. digresses from major theme of lecture', 'answer-1', 34),
('tbi-1', '33. asks if students understand before proceeding to next topic', 'answer-1', 35),
('tbi-1', '34. sticks to the point in answering students’ questions Disclosure: teaching behaviours that serve to clarify course requirements and grading criteria', 'answer-1', 37),
('tbi-1', '35. advises students how to prepare for tests or exams', 'answer-1', 38),
('tbi-1', '36. provides sample exam questions', 'answer-1', 39),
('tbi-1', '37. tells students exactly what is expected on tests, essays, or assignments', 'answer-1', 40),
('tbi-1', '38. states objectives of course as a whole', 'answer-1', 41),
('tbi-1', '39. states objectives of each class session', 'answer-1', 43),
('tbi-1', '40. reminds students of test dates and assignment deadlines Speech: teaching behaviours relating to voice characteristics', 'answer-1', 44),
('tbi-1', '41. stutters, mumbles, or slurs words', 'answer-1', 45),
('tbi-1', '42. speaks at appropriate volume', 'answer-1', 46),
('tbi-1', '43. speaks at appropriate pace', 'answer-1', 47),
('tbi-1', '44. speaks in monotone', 'answer-1', 48),
('tbi-1', '45. speaks clearly \r\nRapport: teaching behaviours that contribute to quality of interpersonal relations between teacher and students', 'answer-1', 50),
('tbi-1', '46. addresses individual students by name', 'answer-1', 51),
('tbi-1', '47. announces availability for consultation outside of class', 'answer-1', 52),
('tbi-1', '48. offers to help students with problems', 'answer-1', 53),
('tbi-1', '49. talks with students before or after clas', 'answer-1', 54),
('tbi-1', '50. shows tolerance of student differences', 'answer-1', 55);

-- --------------------------------------------------------

--
-- Table structure for table `tbi_status`
--

DROP TABLE IF EXISTS `tbi_status`;
CREATE TABLE IF NOT EXISTS `tbi_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL DEFAULT '0',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='TBI Status (Activate Deactivate)' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbi_status`
--

INSERT INTO `tbi_status` (`id`, `status`, `lastupdated`, `user_type`) VALUES
(3, 1, '2015-03-29 04:12:37', 'Teacher'),
(4, 1, '2015-03-29 03:52:31', 'Student');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE IF NOT EXISTS `teachers` (
  `ID` varchar(250) NOT NULL,
  `fname` varchar(250) NOT NULL,
  `midname` varchar(260) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `gender` varchar(250) NOT NULL,
  `addr` varchar(250) NOT NULL,
  `contact` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `bday` varchar(250) NOT NULL,
  `picture` varchar(500) NOT NULL,
  `status` varchar(250) NOT NULL,
  `password` varchar(500) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`ID`, `fname`, `midname`, `lname`, `gender`, `addr`, `contact`, `email`, `bday`, `picture`, `status`, `password`) VALUES
('TCH-1234', 'Roya', 'B.', 'Callope', 'Male', 'asdasdasd', '09265465238', 'roycal@school.com', '2015-02-11', '11005792_385237011661422_68291254_n.jpg', 'Active', 'teacher'),
('TCH-12345', 'test', 'te', 'test', 'Male', 'test', '24234234', 'test@test.com', '2015-02-09', '23.png', 'Unactive', '123'),
('TCH-3112', 'Jerome', 'F', 'Faustino', 'Male', 'Manila', '123333', 'Jerome@gmail.com', '4444-12-31', '1.jpg', 'Active', '123'),
('TCH-4725', 'Rodolfo', 'P', 'Nas', 'Male', 'Muntinlupa City', '091129129', 'ssi.rodolfo.nas@gmail.com', '1991-06-12', '1.jpg', 'Active', '123'),
('TCH-5316', 'Ranni', 'R', 'Rubio', 'Male', 'Laguna', '1233', 'Ranni@gmail.com', '2222-12-31', '1.jpg', 'Active', '123'),
('TCH-5419', 'Jm', 'J', 'Sagrado', 'Male', 'Laguna', '128282872', 'Jm@gmail.com', '232222-12-31', '1.jpg', 'Active', '123'),
('TCH-5507', 'Yen', 'T', 'Manalang', 'Male', 'Makati City', '1233344', 'Yen@gmail.com', '4555-12-31', '1.jpg', 'Active', '123'),
('TCH-7661', 'Aldrin', 'F', 'Calonge', 'Male', 'Ermita, Manila', '09356022424', 'ssi.aldrinl.calonge@gmail.com', '1989-12-05', '1.jpg', 'Active', '123'),
('TCH-7998', 'Russel', 'M.M', 'Macalatan', 'Male', 'Cavite City', '123', 'ssi.russel.macalatan@gmail.com', '4112-12-31', '1.jpg', 'Active', '123'),
('TCH-8040', 'Ron', 'V', 'Lozano', 'Male', 'Cavite City', '1234444', 'ssi.ron.vitug@gmail.com', '1999-12-31', '1.jpg', 'Active', '123'),
('TCH-8883', 'SAMPLE1', 'SAMPLE', 'SAMPLE', 'Male', 'SAMPLE', '23434', 'SAMPLE@SAMPLE.SAMPLE', '2015-02-04', 'Koala.jpg', 'Active', '123');

-- --------------------------------------------------------

--
-- Table structure for table `teachers_class`
--

DROP TABLE IF EXISTS `teachers_class`;
CREATE TABLE IF NOT EXISTS `teachers_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher` text NOT NULL,
  `subject` text NOT NULL,
  `section` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `teachers_class`
--

INSERT INTO `teachers_class` (`id`, `teacher`, `subject`, `section`) VALUES
(84, 'TCH-1234', 'MATH', 'Our Lady of Angels'),
(85, 'TCH-12345', 'SCIENCE', 'Our Lady of Angels'),
(86, 'TCH-1234', 'STEM', 'Our Lady of Angels'),
(90, 'TCH-1234', 'MATH', 'Our Lady of Grace'),
(91, 'TCH-1234', 'SCIENCE', 'Our Lady of Grace'),
(92, 'TCH-12345', 'STEM', 'Our Lady of Grace');

-- --------------------------------------------------------

--
-- Table structure for table `typeoftest`
--

DROP TABLE IF EXISTS `typeoftest`;
CREATE TABLE IF NOT EXISTS `typeoftest` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `typeoftest` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `typeoftest`
--

INSERT INTO `typeoftest` (`id`, `typeoftest`) VALUES
(1, 'FILIPINO INTELLIGENCE TEST'),
(2, 'MANAGEMENT DEVELOPMENT QUESTIONNAIRE'),
(3, 'MD5 MENTAL ABILITY'),
(4, 'MULTI-DIMENSIONAL APTITUDE BATTERY'),
(5, 'PANUKAT NG UGALING PILIPINO'),
(6, 'PURDUE NON-LANGUAGE TEST'),
(7, 'SIX FACTOR PERSONALITY QUESTIONNAIRE'),
(8, 'WATSON GLASSER CRITICAL THINKING TEST FORM A'),
(9, 'WATSON GLASSER CRITICAL THINKING TEST FORM B'),
(10, 'WORKPLACE SKILLS SURVEY'),
(11, 'EMOTIONAL QUOTIENT SURVEY'),
(13, 'SAMPLE');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(250) NOT NULL,
  `user_type` varchar(250) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `firstname`, `lastname`, `user_type`) VALUES
(1, '00001', 'backadmin', 'Admin', 'Account', 'Administrator');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_examsubset`
--
DROP VIEW IF EXISTS `vw_examsubset`;
CREATE TABLE IF NOT EXISTS `vw_examsubset` (
`id` int(10)
,`typeoftest_id` int(10)
,`examsubsetname` varchar(255)
,`timelimit` varchar(6)
,`typeofanswer` varchar(30)
,`typeoftest` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_examtaken`
--
DROP VIEW IF EXISTS `vw_examtaken`;
CREATE TABLE IF NOT EXISTS `vw_examtaken` (
`id` int(10) unsigned
,`examsubset_id` int(10) unsigned
,`status` varchar(10)
,`profiling_id` int(10) unsigned
,`name` varchar(50)
,`gender` varchar(10)
,`age` int(10) unsigned
,`dateoftest` timestamp
,`educ_attaintment` varchar(100)
,`current_employer` varchar(50)
,`current_position` varchar(50)
,`level` varchar(50)
,`level_id` int(10) unsigned
,`typeoftest_id` int(10)
,`examsubsetname` varchar(255)
,`timelimit` varchar(6)
,`typeofanswer` varchar(30)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_examtype`
--
DROP VIEW IF EXISTS `vw_examtype`;
CREATE TABLE IF NOT EXISTS `vw_examtype` (
`id` int(10)
,`category_id` int(10)
,`category` varchar(255)
,`typeoftest_id` int(10)
,`typeoftest` varchar(255)
,`level_id` varchar(100)
,`level` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_studentteacher`
--
DROP VIEW IF EXISTS `vw_studentteacher`;
CREATE TABLE IF NOT EXISTS `vw_studentteacher` (
`id` varchar(250)
,`teacher` text
,`section` text
);
-- --------------------------------------------------------

--
-- Structure for view `vw_examsubset`
--
DROP TABLE IF EXISTS `vw_examsubset`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_examsubset` AS select `examsubset`.`id` AS `id`,`examsubset`.`typeoftest_id` AS `typeoftest_id`,`examsubset`.`examsubsetname` AS `examsubsetname`,`examsubset`.`timelimit` AS `timelimit`,`examsubset`.`typeofanswer` AS `typeofanswer`,`typeoftest`.`typeoftest` AS `typeoftest` from (`examsubset` join `typeoftest` on((`examsubset`.`typeoftest_id` = `typeoftest`.`id`))) order by `examsubset`.`typeoftest_id`,`examsubset`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `vw_examtaken`
--
DROP TABLE IF EXISTS `vw_examtaken`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_examtaken` AS select `examtaken`.`id` AS `id`,`examtaken`.`examsubset_id` AS `examsubset_id`,`examtaken`.`status` AS `status`,`examtaken`.`profiling_id` AS `profiling_id`,`profiling`.`name` AS `name`,`profiling`.`gender` AS `gender`,`profiling`.`age` AS `age`,`profiling`.`dateoftest` AS `dateoftest`,`profiling`.`educ_attaintment` AS `educ_attaintment`,`profiling`.`current_employer` AS `current_employer`,`profiling`.`current_position` AS `current_position`,`profiling`.`level` AS `level`,`profiling`.`level_id` AS `level_id`,`examsubset`.`typeoftest_id` AS `typeoftest_id`,`examsubset`.`examsubsetname` AS `examsubsetname`,`examsubset`.`timelimit` AS `timelimit`,`examsubset`.`typeofanswer` AS `typeofanswer` from ((`examtaken` join `profiling` on((`examtaken`.`profiling_id` = `profiling`.`id`))) join `examsubset` on((`examtaken`.`examsubset_id` = `examsubset`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_examtype`
--
DROP TABLE IF EXISTS `vw_examtype`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_examtype` AS select `examtype`.`id` AS `id`,`examtype`.`category_id` AS `category_id`,`category`.`category` AS `category`,`examtype`.`typeoftest_id` AS `typeoftest_id`,`typeoftest`.`typeoftest` AS `typeoftest`,`examtype`.`level_id` AS `level_id`,`level`.`level` AS `level` from (((`examtype` join `category` on((`examtype`.`category_id` = `category`.`id`))) join `typeoftest` on((`examtype`.`typeoftest_id` = `typeoftest`.`id`))) join `level` on((`examtype`.`level_id` = `level`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `vw_studentteacher`
--
DROP TABLE IF EXISTS `vw_studentteacher`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_studentteacher` AS (select `students`.`ID` AS `id`,`teachers_class`.`teacher` AS `teacher`,`students`.`section` AS `section` from (`students` join `teachers_class` on((`students`.`section` = `teachers_class`.`section`))));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
