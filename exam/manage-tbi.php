<?php
  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<!-- codes starts here -->
<h1><i class="nav-icons fa fa-home"></i>TBI <small>Module</small></h1>
	<form action="proc/update-tbi.php" type="POST">
    <div class="table-responsive">  <table class="table table-condensed"><tbody id="tableResult">
          <tr class="thead" style="font-weight:bold;">
            <td >
            Teacher
        </td>
        </tr>
          <tr>
            <td width="100px">
            <button class="btn btn-info" name= "btn_submit" type="submit" value="T1">Activate</button>
        </td><td>
            <button class="btn btn-info" name= "btn_submit" type="submit" value="T2">Deactivate</button>
        </td></tr>
        <tr><td colspan="2"><hr></hr></td></tr>
         <tr class="thead" style="font-weight:bold;">
            <td>
            Student
        </td>
        </tr>
         <tr><td>
            <button class="btn btn-info" name= "btn_submit" type="submit" value="S1">Activate</button>
        </td><td>
            <button class="btn btn-info" name= "btn_submit" type="submit" value="S2">Deactivate</button>
        </td></tr>
        </tbody></table>
    </div>
  </form>


<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", ":submit", function(e){
      var process = $(this).val();
      switch (process) {
        case 'S1':
          stat = 'Activated';
          user = 'Student';
          break;

        case 'S2':
          stat = 'Dectivated';
          user = 'Student';
          break;
        case 'T1':
          stat = 'Activated';
          user = 'Teacher';
          break;
        case 'T2':
          stat = 'Dectivated';
          user = 'Teacher';
          break;
        default:
          stat = 0;
          user = '';
          break;
      }

    alert(user+ "'s TBI has been "+stat);
});
  });

</script>
  </body>
</html>