<?php
  //include auth
  require_once('auth.php');//include for authorization only
  
  //include config
  require_once('proc/config.php');
  
  include_once('include/include-head.php');//included links here (head)
?>
<!-- codes starts here -->
<h1><i class="nav-icons fa fa-home"></i> Add Batteries<small></small></h1>


     <div class="panel panel-default output">
        <div class="panel-heading">
          <h3>Exam Information</h3>
        </div>
        <div class="panel-body">
          <!-- CONTENT BODY HERE -->
          <div class="row">

              <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                     <p><b>Note </b></p>
                     <p>- SET can be multiple, just add comma(,) for each set.</p> 
                     <p>- The first SET will be the first one typed.</p>
                     <p>- If there's no SET just leave it blank.</p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>Level:</label>
                  <input type="text" class="form-control" name="position" />
                </div>
                
                <div class="datalist" style="height:250px;overflow:scroll;overflow-x:hidden;">
                  <h4>Added Level</h4>

                </div>
            
          </div>
        </div>
        <div class="panel-footer">
            <div class="row">
              <div class="col-md-12">
                <button class="btn btn-primary btn-md pull-right" id="btnSave" data-toggle="tooltip" title="Click to add" data-placement="left">Add</button>          
              </div>
            </div>
        </div>
</div>   


      <!-- MODAL -->
      <div id="modal-alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
          <div class="modal-content text-center">
            <br />
            <img id="loading" src="image/loading.gif" />
            <h1 id="loading-text" class="modal-title">Saving...</h1>
          </div>
        </div>
      </div>
      <!-- MODAL -->


<!-- codes ends here -->
<?php 
  include_once('include/include-body.php');//included links here (body) 
?>

	<script type="text/javascript" src="js/add-level.js"></script>


  </body>
</html>